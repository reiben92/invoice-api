<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

use App\Models\Clients as Client;


class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients',function (Blueprint $table){
            $table->increments('client_id');
            $table->longText('client_name');
            $table->longText('client_secret');
            $table->timestamps();
            $table->softDeletes();
        });

        $Clients = new Client;
        $Clients->client_name='Frontend';
        $Clients->client_secret = Hash::make('password');
        $Clients->save();

        $Clients = new Client;
        $Clients->client_name='Frontend2';
        $Clients->client_secret = Hash::make('password2');
        $Clients->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropifExists('clients');
    }
}
