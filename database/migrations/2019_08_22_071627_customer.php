<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers',function (Blueprint $table){
            $table->increments('id');
            $table->longText('customer_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('customer_balances',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();;
            $table->index('customer_id');
            $table->decimal('current_balance',20,5)->nullable();
            $table->longText('current_balance_date')->nullable();
            $table->decimal('open_balance',20,5)->nullable();
            $table->longText('open_balance_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('customer_details',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();
            $table->index('customer_id');

            $table->longText('customer_name')->nullable();
            $table->longText('type')->nullable();
            $table->longText('nickname')->nullable();
            
            $table->unsignedInteger('default_currency')->nullable();
            $table->index('default_currency');
            $table->longText('tax_registration_no')->nullable();
            $table->longText('roc_no')->nullable();
            $table->longText('email')->nullable();
            $table->longText('website')->nullable();
            $table->longText('fax')->nullable();
            $table->longText('mobile_no')->nullable();
            $table->longText('phone_no')->nullable();

            $table->longText('default_payment')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('customer_contacts',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();;
            $table->index('customer_id');

            $table->longText('title')->nullable();
            $table->longText('first_name')->nullable();
            $table->longText('last_name')->nullable();
            $table->longText('email')->nullable();
            $table->longText('website')->nullable();
            $table->longText('fax')->nullable();
            $table->longText('mobile_no')->nullable();
            $table->longText('phone_no')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('customer_addresses',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();;
            $table->index('customer_id');

            $table->longText('billing_address1')->nullable();
            $table->longText('billing_address2')->nullable();
            $table->longText('billing_address3')->nullable();
            $table->longText('billing_city')->nullable();
            $table->longText('billing_postcode')->nullable();
            $table->longText('billing_state')->nullable();
            $table->longText('billing_country')->nullable();
            $table->unsignedInteger('shipping_is_same_as_billing')->nullable();
            $table->longText('shipping_address1')->nullable();
            $table->longText('shipping_address2')->nullable();
            $table->longText('shipping_address3')->nullable();
            $table->longText('shipping_city')->nullable();
            $table->longText('shipping_postcode')->nullable();
            $table->longText('shipping_state')->nullable();
            $table->longText('shipping_country')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $arr = ['customers','customer_addresses','customer_details','customer_contacts','customer_balances'];

        foreach($arr as $ar){
            Schema::dropifExists($ar);
        }
    }
}
