<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Counters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counters',function (Blueprint $table){
            $table->increments('id');
            $table->longText('type')->nullable();
            $table->longText('prefix')->nullable();
            $table->longText('pre')->nullable();
            $table->decimal('value',20,2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropifExists('counters');
    }
}
