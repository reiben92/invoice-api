<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Invoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::create('invoice_recurring',function (Blueprint $table){
            $table->increments('id');
            $table->longText('interval')->nullable();
            $table->longText('interval_value')->nullable();
            $table->longText('payment_terms')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_recurring_pivot',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_recurring_id')->nullable();
            $table->unsignedInteger('invoice_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('invoices',function (Blueprint $table){
            $table->increments('id');
            $table->longText('invoice_no')->nullable();
            $table->unsignedInteger('invoice_status_id')->nullable();
            $table->index('invoice_status_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_statement_of_account',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->index('invoice_id');
            $table->decimal('last_payment',20,5)->default(0)->nullable();
            $table->decimal('previous_balance',20,5)->default(0)->nullable();
            $table->decimal('current_balance',20,5)->default(0)->nullable();
            $table->decimal('new_charges',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_summary',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->index('invoice_id');
            $table->decimal('sub_total',20,5)->default(0)->nullable();
            $table->longText('discount_type')->nullable();
            $table->decimal('discount_amount',20,5)->default(0)->nullable();
            $table->decimal('total_discount',20,5)->default(0)->nullable();
            $table->decimal('total_tax',20,5)->default(0)->nullable();
            $table->decimal('grand_total',20,5)->default(0)->nullable();
            $table->decimal('balance_due',20,5)->default(0)->nullable();
            $table->unsignedInteger('currency_id')->nullable();
            $table->index('currency_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_purchase_order',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id')->nullable();
            $table->unsignedInteger('purchase_order_id')->nullable();
            $table->longText('purchase_order_no')->nullable();
            $table->index('invoice_id');
            $table->index('purchase_order_id');
            $table->timestamps(); $table->softDeletes();
        });

        Schema::create('invoice_quotation',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id')->nullable();
            $table->unsignedInteger('quotation_id')->nullable();
            $table->longText('quotation_no')->nullable();
            $table->index('quotation_id');
            $table->index('invoice_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_items',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->index('invoice_id');

            $table->longText('package_type')->nullable();
            
            $table->longText('package_name')->nullable();
            $table->longText('description')->nullable();
            
            $table->unsignedInteger('invoice_type_id')->nullable();
            $table->index('invoice_type_id');
            
            $table->unsignedInteger('package_id')->nullable();
            $table->index('package_id');

            $table->unsignedInteger('product_id')->nullable();
            $table->index('product_id');

            $table->decimal('quantity',20,5)->default(0)->nullable();
            $table->decimal('rate',20,5)->default(0)->nullable();
            $table->decimal('amount',20,5)->default(0)->nullable();
            $table->decimal('discount',20,5)->default(0)->nullable();
            $table->decimal('grand_total',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_items_taxes',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->index('invoice_id');

            $table->unsignedInteger('invoice_item_id');
            $table->index('invoice_item_id');

            $table->unsignedInteger('tax_type_id')->nullable();
            $table->index('tax_type_id');
            $table->decimal('amount',20,5)->default(0)->nullable();
            $table->decimal('tax_rate',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_details',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->index('invoice_id');
            $table->longText('billing_contact_first_name')->nullable();
            $table->longText('billing_contact_last_name')->nullable();
            $table->longText('billing_email')->nullable();
            $table->longText('invoice_date')->nullable();
            $table->longText('payment_due')->nullable();
            $table->longText('invoice_description')->nullable();
            $table->longText('terms')->nullable();

            $table->longText('billing_address1')->nullable();
            $table->longText('billing_address2')->nullable();
            $table->longText('billing_address3')->nullable();
            $table->longText('billing_city')->nullable();
            $table->longText('billing_postcode')->nullable();
            $table->longText('billing_state')->nullable();
            $table->longText('billing_country')->nullable();

            $table->longText('shipping_address1')->nullable();
            $table->longText('shipping_address2')->nullable();
            $table->longText('shipping_address3')->nullable();
            $table->longText('shipping_city')->nullable();
            $table->longText('shipping_postcode')->nullable();
            $table->longText('shipping_state')->nullable();
            $table->longText('shipping_country')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_types',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->longText('slug')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoice_statuses',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });


        Schema::create('taxes',function(Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->longText('description')->nullable();
            $table->decimal('default_tax_rate',20,2)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('taxes_accounting_code',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('tax_id')->nullable();
            $table->unsignedInteger('accounting_id')->nullable();
            $table->index('tax_id');
            $table->index('accounting_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('currencies',function(Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->longText('code')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('invoices_customer',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();
            $table->unsignedInteger('invoice_id')->nullable();
            $table->index('customer_id');
            $table->index('invoice_id');
            $table->timestamps(); 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        $arr = ['invoices','invoice_types','invoice_items','invoice_items_taxes','invoice_details','invoice_statuses','invoice_statement_of_account','taxes','currencies','invoices_customer','invoice_summary','invoice_purchase_order','invoice_quotation','taxes_accounting_code','invoice_recurring','invoice_recurring_pivot'];

        foreach($arr as $ar){
            Schema::dropifExists($ar);
        }
    }
}
