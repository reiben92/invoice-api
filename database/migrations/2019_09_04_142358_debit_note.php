<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DebitNote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit_notes',function (Blueprint $table){
            $table->increments('id');
            $table->longText('debit_note_no')->nullable();

            $table->unsignedInteger('debit_note_status_id')->nullable();
            $table->index('debit_note_status_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_summary',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('debit_note_id');
            $table->index('debit_note_id');
            $table->decimal('sub_total',20,5)->default(0)->nullable();
            $table->longText('discount_type')->nullable();
            $table->decimal('discount_amount',20,5)->default(0)->nullable();
            $table->decimal('total_discount',20,5)->default(0)->nullable();
            $table->decimal('total_tax',20,5)->default(0)->nullable();
            $table->decimal('grand_total',20,5)->default(0)->nullable();
            $table->decimal('balance_due',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_items',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('debit_note_id');
            $table->index('debit_note_id');

            $table->longText('package_type')->nullable();
            
            $table->longText('package_name')->nullable();
            $table->longText('description')->nullable();
            
            $table->unsignedInteger('debit_note_type')->nullable();
            $table->index('debit_note_type');

            $table->unsignedInteger('debit_note_type_id')->nullable();
            $table->index('debit_note_type_id');
            
            $table->unsignedInteger('package_id')->nullable();
            $table->index('package_id');

            $table->unsignedInteger('product_id')->nullable();
            $table->index('product_id');

            $table->decimal('quantity',20,5)->default(0)->nullable();
            $table->decimal('rate',20,5)->default(0)->nullable();
            $table->decimal('amount',20,5)->default(0)->nullable();
            $table->decimal('discount',20,5)->default(0)->nullable();
            $table->decimal('grand_total',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_items_taxes',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('debit_note_id');
            $table->index('debit_note_id');

            $table->unsignedInteger('debit_note_item_id');
            $table->index('debit_note_item_id');

            $table->unsignedInteger('tax_type_id')->nullable();
            $table->index('tax_type_id');
            $table->decimal('amount',20,5)->default(0)->nullable();
            $table->decimal('tax_rate',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_details',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('debit_note_id');
            $table->index('debit_note_id');
            $table->longText('billing_contact_first_name')->nullable();
            $table->longText('billing_contact_last_name')->nullable();
            $table->longText('billing_email')->nullable();
            $table->longText('debit_note_date')->nullable();
            $table->longText('payment_due')->nullable();
            $table->longText('debit_note_description')->nullable();
            $table->longText('terms')->nullable();

            $table->longText('billing_address1')->nullable();
            $table->longText('billing_address2')->nullable();
            $table->longText('billing_address3')->nullable();
            $table->longText('billing_city')->nullable();
            $table->longText('billing_postcode')->nullable();
            $table->longText('billing_state')->nullable();
            $table->longText('billing_country')->nullable();

            $table->longText('shipping_address1')->nullable();
            $table->longText('shipping_address2')->nullable();
            $table->longText('shipping_address3')->nullable();
            $table->longText('shipping_city')->nullable();
            $table->longText('shipping_postcode')->nullable();
            $table->longText('shipping_state')->nullable();
            $table->longText('shipping_country')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_types',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->longText('slug')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_statuses',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_currency',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('debit_note_id');
            $table->index('debit_note_id');
            $table->unsignedInteger('currency_id')->nullable();
            $table->index('currency_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('debit_note_customer',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();
            $table->unsignedInteger('debit_note_id')->nullable();
            $table->index('customer_id');
            $table->index('debit_note_id');
            $table->timestamps(); 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $arr = ['debit_notes','debit_note_types','debit_note_items','debit_note_summary','debit_note_details','debit_note_customer','debit_note_summary','debit_note_statuses','debit_note_items_taxes'];

        foreach($arr as $ar){
            Schema::dropifExists($ar);
        }
    }
}
