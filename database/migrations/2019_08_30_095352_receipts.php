<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Receipts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts',function (Blueprint $table){
            $table->increments('id');
            $table->longText('receipt_no')->nullable();

            $table->unsignedInteger('receipt_status_id')->nullable();
            $table->index('receipt_status_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('receipt_summary',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('receipt_id')->nullable();
            $table->index('receipt_id');
            $table->decimal('sub_total',20,5)->default(0)->nullable();
            $table->decimal('total_tax',20,5)->default(0)->nullable();
            $table->decimal('grand_total',20,5)->default(0)->nullable();
            $table->decimal('balance_due',20,5)->default(0)->nullable();
            $table->unsignedInteger('currency_id')->nullable();
            $table->index('currency_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('receipt_items',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('invoice_id')->nullable();
            $table->index('invoice_id');
            $table->unsignedInteger('receipt_id')->nullable();
            $table->index('receipt_id');

            $table->longText('package_type')->nullable();
            
            $table->longText('package_name')->nullable();
            $table->longText('description')->nullable();
            
            $table->unsignedInteger('receipt_type_id')->nullable();
            $table->index('receipt_type_id');
            
            $table->unsignedInteger('package_id')->nullable();
            $table->index('package_id');

            $table->unsignedInteger('product_id')->nullable();
            $table->index('product_id');

            $table->decimal('quantity',20,5)->default(0)->nullable();
            $table->decimal('rate',20,5)->default(0)->nullable();
            $table->decimal('amount',20,5)->default(0)->nullable();
            $table->decimal('grand_total',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('receipt_items_taxes',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('receipt_id')->nullable();
            $table->index('receipt_id');

            $table->unsignedInteger('receipt_item_id')->nullable();
            $table->index('receipt_item_id');

            $table->unsignedInteger('tax_type_id')->nullable();
            $table->index('tax_type_id');
            $table->decimal('amount',20,5)->default(0)->nullable();
            $table->decimal('tax_rate',20,5)->default(0)->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('receipt_details',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('receipt_id')->nullable();
            $table->index('receipt_id');
            $table->longText('billing_contact_first_name')->nullable();
            $table->longText('billing_contact_last_name')->nullable();
            $table->longText('billing_email')->nullable();
            
            $table->longText('receipt_description')->nullable();
            $table->longText('receipt_date')->nullable();

            $table->longText('billing_address1')->nullable();
            $table->longText('billing_address2')->nullable();
            $table->longText('billing_address3')->nullable();
            $table->longText('billing_city')->nullable();
            $table->longText('billing_postcode')->nullable();
            $table->longText('billing_state')->nullable();
            $table->longText('billing_country')->nullable();

            $table->longText('shipping_address1')->nullable();
            $table->longText('shipping_address2')->nullable();
            $table->longText('shipping_address3')->nullable();
            $table->longText('shipping_city')->nullable();
            $table->longText('shipping_postcode')->nullable();
            $table->longText('shipping_state')->nullable();
            $table->longText('shipping_country')->nullable();

            $table->unsignedInteger('payment_method_id')->nullable();
            $table->unsignedInteger('bank_account_id')->nullable();
            $table->index('payment_method_id');
            $table->index('bank_account_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('receipt_types',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('receipt_statuses',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('receipt_customers',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable();
            $table->unsignedInteger('receipt_id')->nullable();
            $table->index('receipt_id');
            $table->index('customer_id');
            $table->timestamps(); 
            $table->softDeletes();
        });

        Schema::create('payment_methods',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->unsignedInteger('accounting_id')->nullable();
            $table->index('accounting_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('bank_accounts',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->longText('value')->nullable();
            $table->unsignedInteger('accounting_id')->nullable();
            $table->index('accounting_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        $arr = ['receipts','receipt_types','receipt_items','receipt_items_taxes','receipt_details','receipt_statuses','receipt_customers','receipt_summary','payment_methods','bank_accounts'];

        foreach($arr as $ar){
            Schema::dropifExists($ar);
        }
    }
}
