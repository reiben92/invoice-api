<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_services',function (Blueprint $table){
        	$table->increments('id');
            $table->unsignedInteger('status_id')->nullable();
            $table->index('status_id');
            $table->timestamps();
        	$table->softDeletes();
        });

        Schema::create('product_services_details',function (Blueprint $table){
        	$table->increments('id');
        	$table->unsignedInteger('product_id')->nullable();
        	$table->index('product_id');
            $table->unsignedInteger('product_type_id')->nullable();
            $table->index('product_type_id');
        	$table->longText('product_code')->nullable();
            $table->longText('product_name')->nullable();
            $table->longText('product_description')->nullable();
        	$table->timestamps();
        	$table->softDeletes();
        });

        Schema::create('product_services_accounting',function (Blueprint $table){
        	$table->increments('id');
        	$table->unsignedInteger('product_id')->nullable();
        	$table->index('product_id');

        	$table->unsignedInteger('product_detail_id')->nullable();
        	$table->index('product_detail_id');

            $table->unsignedInteger('accounting_id')->nullable();
            $table->index('accounting_id');        	

        	$table->timestamps();
        	$table->softDeletes();
        });

        Schema::create('accounting_details',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_services_status',function (Blueprint $table){
        	$table->increments('id');
        	$table->longText('title')->nullable();
        	$table->timestamps();
        	$table->softDeletes();
        });

        Schema::create('product_services_api',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('product_services_api_id')->nullable();
            $table->index('product_services_api_id');
            $table->longText('api_from')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_services_api_details',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('product_services_id')->nullable();
            $table->unsignedInteger('product_services_api_id')->nullable();

            $table->index('product_services_id');
            $table->index('product_services_api_id');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_services_types',function (Blueprint $table){
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $arr = ['product_services','product_services_details','product_services_accounting','accounting_details','product_services_status','producproduct_services_typest_types','product_services_api','product_services_api_details'];

        foreach($arr as $ar){
            Schema::dropifExists($ar);
        }
    }
}
