<?php

use Illuminate\Database\Seeder;

use App\Models\CreditNoteStatuses;
use App\Models\DebitNoteStatuses;

use App\Models\CreditNoteTypes;
use App\Models\DebitNoteTypes;

class CreditDebitNoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$Statuses = ['Draft','Submitted','Approved','Rejected'];
    	foreach($Statuses as $status){
    		$s1 = new CreditNoteStatuses;
    		$s1->title = $status;
    		$s1->save();

            $s2 = new DebitNoteStatuses;
            $s2->title = $status;
            $s2->save();
    	}



    	$Types = [['title'=>'Invoices','slug'=>'invoices'],
                  ['title'=>'Receipts','slug'=>'receipts'],
                  ['title'=>'Credit Note','slug'=>'credit_note'],
                  ['title'=>'Debit Note','slug'=>'debit_note'],
                ];
    	foreach($Types as $type){
    		$t1 = new CreditNoteTypes;
    		$t1->title = $type['title'];
            $t1->slug = $type['slug'];
    		$t1->save();

            $t2 = new DebitNoteTypes;
            $t2->title = $type['title'];
            $t2->slug = $type['slug'];
            $t2->save();
    	}
    }
}
