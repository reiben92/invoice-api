<?php

use Illuminate\Database\Seeder;

use App\Models\Currencies;
use App\Models\Invoices;
use App\Models\InvoiceItems;
use App\Models\InvoiceTypes;
use App\Models\InvoiceStatuses;
use App\Models\InvoiceCurrency;
use App\Models\InvoiceDetails;
use App\Models\InvoiceItemsTaxes;
use App\Models\Taxes;
use App\Models\InvoiceStatementOfAccount;
use App\Models\InvoiceSummary;
use App\Models\InvoicesCustomer;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	

    	$Taxes = [
    				['title'=>'SST','default_tax_rate'=>6, 'description'=>'Sales Services Tax'],
    				['title'=>'GST','default_tax_rate'=>6, 'description'=>'Governmenet Sales Tax']
    			];
    	foreach($Taxes as $tax){
        	$t = new Taxes;
        	$t->title = $tax['title'];
        	$t->default_tax_rate = $tax['default_tax_rate'];
        	$t->description = $tax['description'];
        	$t->save();
    	}

    	$Currencies = array (
					  'AED' => 'United Arab Emirates Dirham',
					  'AFN' => 'Afghan Afghani',
					  'ALL' => 'Albanian Lek',
					  'AMD' => 'Armenian Dram',
					  'ANG' => 'Netherlands Antillean Guilder',
					  'AOA' => 'Angolan Kwanza',
					  'ARS' => 'Argentine Peso',
					  'AUD' => 'Australian Dollar',
					  'AWG' => 'Aruban Florin',
					  'AZN' => 'Azerbaijani Manat',
					  'BAM' => 'Bosnia-Herzegovina Convertible Mark',
					  'BBD' => 'Barbadian Dollar',
					  'BDT' => 'Bangladeshi Taka',
					  'BGN' => 'Bulgarian Lev',
					  'BHD' => 'Bahraini Dinar',
					  'BIF' => 'Burundian Franc',
					  'BMD' => 'Bermudan Dollar',
					  'BND' => 'Brunei Dollar',
					  'BOB' => 'Bolivian Boliviano',
					  'BRL' => 'Brazilian Real',
					  'BSD' => 'Bahamian Dollar',
					  'BTC' => 'Bitcoin',
					  'BTN' => 'Bhutanese Ngultrum',
					  'BWP' => 'Botswanan Pula',
					  'BYN' => 'Belarusian Ruble',
					  'BZD' => 'Belize Dollar',
					  'CAD' => 'Canadian Dollar',
					  'CDF' => 'Congolese Franc',
					  'CHF' => 'Swiss Franc',
					  'CLF' => 'Chilean Unit of Account (UF)',
					  'CLP' => 'Chilean Peso',
					  'CNH' => 'Chinese Yuan (Offshore)',
					  'CNY' => 'Chinese Yuan',
					  'COP' => 'Colombian Peso',
					  'CRC' => 'Costa Rican Colón',
					  'CUC' => 'Cuban Convertible Peso',
					  'CUP' => 'Cuban Peso',
					  'CVE' => 'Cape Verdean Escudo',
					  'CZK' => 'Czech Republic Koruna',
					  'DJF' => 'Djiboutian Franc',
					  'DKK' => 'Danish Krone',
					  'DOP' => 'Dominican Peso',
					  'DZD' => 'Algerian Dinar',
					  'EGP' => 'Egyptian Pound',
					  'ERN' => 'Eritrean Nakfa',
					  'ETB' => 'Ethiopian Birr',
					  'EUR' => 'Euro',
					  'FJD' => 'Fijian Dollar',
					  'FKP' => 'Falkland Islands Pound',
					  'GBP' => 'British Pound Sterling',
					  'GEL' => 'Georgian Lari',
					  'GGP' => 'Guernsey Pound',
					  'GHS' => 'Ghanaian Cedi',
					  'GIP' => 'Gibraltar Pound',
					  'GMD' => 'Gambian Dalasi',
					  'GNF' => 'Guinean Franc',
					  'GTQ' => 'Guatemalan Quetzal',
					  'GYD' => 'Guyanaese Dollar',
					  'HKD' => 'Hong Kong Dollar',
					  'HNL' => 'Honduran Lempira',
					  'HRK' => 'Croatian Kuna',
					  'HTG' => 'Haitian Gourde',
					  'HUF' => 'Hungarian Forint',
					  'IDR' => 'Indonesian Rupiah',
					  'ILS' => 'Israeli New Sheqel',
					  'IMP' => 'Manx pound',
					  'INR' => 'Indian Rupee',
					  'IQD' => 'Iraqi Dinar',
					  'IRR' => 'Iranian Rial',
					  'ISK' => 'Icelandic Króna',
					  'JEP' => 'Jersey Pound',
					  'JMD' => 'Jamaican Dollar',
					  'JOD' => 'Jordanian Dinar',
					  'JPY' => 'Japanese Yen',
					  'KES' => 'Kenyan Shilling',
					  'KGS' => 'Kyrgystani Som',
					  'KHR' => 'Cambodian Riel',
					  'KMF' => 'Comorian Franc',
					  'KPW' => 'North Korean Won',
					  'KRW' => 'South Korean Won',
					  'KWD' => 'Kuwaiti Dinar',
					  'KYD' => 'Cayman Islands Dollar',
					  'KZT' => 'Kazakhstani Tenge',
					  'LAK' => 'Laotian Kip',
					  'LBP' => 'Lebanese Pound',
					  'LKR' => 'Sri Lankan Rupee',
					  'LRD' => 'Liberian Dollar',
					  'LSL' => 'Lesotho Loti',
					  'LYD' => 'Libyan Dinar',
					  'MAD' => 'Moroccan Dirham',
					  'MDL' => 'Moldovan Leu',
					  'MGA' => 'Malagasy Ariary',
					  'MKD' => 'Macedonian Denar',
					  'MMK' => 'Myanma Kyat',
					  'MNT' => 'Mongolian Tugrik',
					  'MOP' => 'Macanese Pataca',
					  'MRO' => 'Mauritanian Ouguiya (pre-2018)',
					  'MRU' => 'Mauritanian Ouguiya',
					  'MUR' => 'Mauritian Rupee',
					  'MVR' => 'Maldivian Rufiyaa',
					  'MWK' => 'Malawian Kwacha',
					  'MXN' => 'Mexican Peso',
					  'MYR' => 'Malaysian Ringgit',
					  'MZN' => 'Mozambican Metical',
					  'NAD' => 'Namibian Dollar',
					  'NGN' => 'Nigerian Naira',
					  'NIO' => 'Nicaraguan Córdoba',
					  'NOK' => 'Norwegian Krone',
					  'NPR' => 'Nepalese Rupee',
					  'NZD' => 'New Zealand Dollar',
					  'OMR' => 'Omani Rial',
					  'PAB' => 'Panamanian Balboa',
					  'PEN' => 'Peruvian Nuevo Sol',
					  'PGK' => 'Papua New Guinean Kina',
					  'PHP' => 'Philippine Peso',
					  'PKR' => 'Pakistani Rupee',
					  'PLN' => 'Polish Zloty',
					  'PYG' => 'Paraguayan Guarani',
					  'QAR' => 'Qatari Rial',
					  'RON' => 'Romanian Leu',
					  'RSD' => 'Serbian Dinar',
					  'RUB' => 'Russian Ruble',
					  'RWF' => 'Rwandan Franc',
					  'SAR' => 'Saudi Riyal',
					  'SBD' => 'Solomon Islands Dollar',
					  'SCR' => 'Seychellois Rupee',
					  'SDG' => 'Sudanese Pound',
					  'SEK' => 'Swedish Krona',
					  'SGD' => 'Singapore Dollar',
					  'SHP' => 'Saint Helena Pound',
					  'SLL' => 'Sierra Leonean Leone',
					  'SOS' => 'Somali Shilling',
					  'SRD' => 'Surinamese Dollar',
					  'SSP' => 'South Sudanese Pound',
					  'STD' => 'São Tomé and Príncipe Dobra (pre-2018)',
					  'STN' => 'São Tomé and Príncipe Dobra',
					  'SVC' => 'Salvadoran Colón',
					  'SYP' => 'Syrian Pound',
					  'SZL' => 'Swazi Lilangeni',
					  'THB' => 'Thai Baht',
					  'TJS' => 'Tajikistani Somoni',
					  'TMT' => 'Turkmenistani Manat',
					  'TND' => 'Tunisian Dinar',
					  'TOP' => 'Tongan Pa\'anga',
					  'TRY' => 'Turkish Lira',
					  'TTD' => 'Trinidad and Tobago Dollar',
					  'TWD' => 'New Taiwan Dollar',
					  'TZS' => 'Tanzanian Shilling',
					  'UAH' => 'Ukrainian Hryvnia',
					  'UGX' => 'Ugandan Shilling',
					  'USD' => 'United States Dollar',
					  'UYU' => 'Uruguayan Peso',
					  'UZS' => 'Uzbekistan Som',
					  'VEF' => 'Venezuelan Bolívar Fuerte',
					  'VND' => 'Vietnamese Dong',
					  'VUV' => 'Vanuatu Vatu',
					  'WST' => 'Samoan Tala',
					  'XAF' => 'CFA Franc BEAC',
					  'XAG' => 'Silver Ounce',
					  'XAU' => 'Gold Ounce',
					  'XCD' => 'East Caribbean Dollar',
					  'XDR' => 'Special Drawing Rights',
					  'XOF' => 'CFA Franc BCEAO',
					  'XPD' => 'Palladium Ounce',
					  'XPF' => 'CFP Franc',
					  'XPT' => 'Platinum Ounce',
					  'YER' => 'Yemeni Rial',
					  'ZAR' => 'South African Rand',
					  'ZMW' => 'Zambian Kwacha',
					  'ZWL' => 'Zimbabwean Dollar',
					);

		$keys = array_keys($Currencies);

    	foreach($keys as $key){
        	$c = new Currencies;
        	$c->title = $Currencies[$key];
        	$c->code = $key;
        	$c->save();
    	}

    	$Statuses = ['Draft','Open','Closed'];
    	foreach($Statuses as $status){
    		$s = new InvoiceStatuses;
    		$s->title = $status;
    		$s->save();
    	}

    	$Types = ['Installation Charges','Additional Charges','Deposit','Activation Fee','Charge Amount (Package)'];
    	foreach($Types as $type){
    		$t = new InvoiceTypes;
    		$t->title = $type;
    		$t->save();
    	}

		/*for($i=0;$i<=3036;$i++){
			$Invoices1 = new Invoices;
			$Invoices1->invoice_no ='I000000000'.(Invoices::count()+1);
			$Invoices1->invoice_status_id = 2;
			$Invoices1->save();

			$InvoicesCustomer1 = new InvoicesCustomer;
			$InvoicesCustomer1->customer_id = 1;
			$InvoicesCustomer1->invoice_id = $Invoices1->id;
			$InvoicesCustomer1->save();

			$InvoicesCurrency1 = new InvoiceCurrency;
			$InvoicesCurrency1->currency_id = 1;
			$InvoicesCurrency1->invoice_id = $Invoices1->id;
			$InvoicesCurrency1->save();

			$InvoiceDetails1 = new InvoiceDetails;
			$InvoiceDetails1->invoice_id = $Invoices1->id;
			$InvoiceDetails1->save();

			$InvoiceItems1 = new InvoiceItems;
			$InvoiceItems1->invoice_id = $Invoices1->id;
			$InvoiceItems1->package_type = 'Broadband';
			$InvoiceItems1->package_name = '100 MBPS Unifi';
			$InvoiceItems1->description = 'Standard Monthly Invoice';
			$InvoiceItems1->invoice_type_id = 5;
			$InvoiceItems1->product_id = 1;
			$InvoiceItems1->amount = 200;
			$InvoiceItems1->discount = 0;
			$InvoiceItems1->grand_total = 212.00;
			$InvoiceItems1->save();

			$InvoiceItemTaxes1 = new InvoiceItemsTaxes;
			$InvoiceItemTaxes1->invoice_id = $Invoices1->id;
			$InvoiceItemTaxes1->invoice_item_id = $InvoiceItems1->id;
			$InvoiceItemTaxes1->tax_type_id = 1;
			$InvoiceItemTaxes1->amount = 12.00;
			$InvoiceItemTaxes1->tax_rate = 6;
			$InvoiceItemTaxes1->save();

			$InvoiceItems2 = new InvoiceItems;
			$InvoiceItems2->invoice_id = $Invoices1->id;
			$InvoiceItems2->package_type = 'Broadband';
			$InvoiceItems2->package_name = '100 MBPS Unifi';
			$InvoiceItems2->description = 'Deposit';
			$InvoiceItems2->invoice_type_id = 3;
			$InvoiceItems2->package_id = 1;
			$InvoiceItems2->amount = 200;
			$InvoiceItems2->discount = 0;
			$InvoiceItems2->grand_total = 200.00;
			$InvoiceItems2->save();

			$InvoiceItems3 = new InvoiceItems;
			$InvoiceItems3->invoice_id = $Invoices1->id;
			$InvoiceItems3->package_type = 'Broadband';
			$InvoiceItems3->package_name = '100 MBPS Unifi';
			$InvoiceItems3->description = 'Activation Fee';
			$InvoiceItems3->invoice_type_id = 4;
			$InvoiceItems3->package_id = 1;
			$InvoiceItems3->amount = 300;
			$InvoiceItems3->discount = 0;
			$InvoiceItems3->grand_total = 318.00;
			$InvoiceItems3->save();

			$InvoiceItemTaxes3 = new InvoiceItemsTaxes;
			$InvoiceItemTaxes3->invoice_id = $Invoices1->id;
			$InvoiceItemTaxes3->invoice_item_id = $InvoiceItems3->id;
			$InvoiceItemTaxes3->tax_type_id = 1;
			$InvoiceItemTaxes3->amount = 18.00;
			$InvoiceItemTaxes3->tax_rate = 6;
			$InvoiceItemTaxes3->save();

			$InvoiceStatementOfAccount = new InvoiceStatementOfAccount;
			$InvoiceStatementOfAccount->new_charges = 730.00;
			$InvoiceStatementOfAccount->invoice_id = $Invoices1->id;
			$InvoiceStatementOfAccount->save();
		}*/
    }
}
