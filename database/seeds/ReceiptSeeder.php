<?php

use Illuminate\Database\Seeder;

use App\Models\ReceiptStatuses;
use App\Models\ReceiptTypes;
use App\Models\PaymentMethods;
use App\Models\BankAccounts;

class ReceiptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$Statuses = ['Open','Closed'];
    	foreach($Statuses as $status){
    		$s = new ReceiptStatuses;
    		$s->title = $status;
    		$s->save();
    	}

    	$Types = ['Normal Payment','Advance Payment','Deposit'];
    	foreach($Types as $type){
    		$t = new ReceiptTypes;
    		$t->title = $type;
    		$t->save();
    	}

        $CC = ['Bank Transfer','Credit Card','Debit Card','Cash','Cheque'];
        foreach($CC as $cc){
            $c = new PaymentMethods;
            $c->title = $cc;
            $c->save();
        }

        $BB = array(['title'=>'CIMB','value'=>1000000000],
                    ['title'=>'Maybank','value'=>2000000000],
                    ['title'=>'RHB','value'=>3000000000],
                    ['title'=>'Affin Islamic', 'value'=>4000000000],
                    );

        foreach($BB as $bb){
            $b = new BankAccounts;
            $b->title = $bb['title'];
            $b->value = $bb['value'];
            $b->save();
        }
    }
}
