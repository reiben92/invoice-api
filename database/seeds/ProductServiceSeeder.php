<?php

use Illuminate\Database\Seeder;

use App\Models\ProductServices;
use App\Models\ProductServicesDetails;
use App\Models\ProductServicesAccounting;
use App\Models\AccountingDetails;
use App\Models\ProductServicesStatus;
use App\Models\ProductServicesTypes;

class ProductServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$data = [
    				 	['product_code'=>'P0000000001',
                         'product_name'=>'Broadband',
                         'accounting_id'=>'1',
                         'product_description'=>'Internet',
                         'product_type_id'=>'1'
    				 	],
    				 	['product_code'=>'P0000000002',
                         'product_name'=>'Installation',
                         'accounting_id'=>'2',
                         'product_description'=>'Installation',
                         'product_type_id'=>'1'
                        ],
                        ['product_code'=>'P0000000003',
                         'product_name'=>'Router',
                         'accounting_id'=>'3',
                         'product_description'=>'Equipment',
                         'product_type_id'=>'2'
                        ],
    				 ];

    	

    	foreach($data as $dat){
    		$p = new ProductServices;
            $p->status_id = 1;
    		$p->save();

            $p_details = new ProductServicesDetails;
            $p_details->product_id=$p->id;
            $p_details->product_code=$dat['product_code'];
            $p_details->product_name=$dat['product_name'];
            $p_details->product_description = $dat['product_description'];
            $p_details->product_type_id = $dat['product_type_id'];
            $p_details->save();

            $p_accounting_details = new ProductServicesAccounting;
            $p_accounting_details->product_detail_id = $p_details->id;
            $p_accounting_details->product_id = $p->id;
            $p_accounting_details->accounting_id = $dat['accounting_id'];
            $p_accounting_details->save();
    	}

        $Statuses = [
                        ['title'=>'Active'],
                        ['title'=>'Inactive']
                    ];

        foreach($Statuses as $status){
            $s = new ProductServicesStatus;
            $s->title = $status['title'];
            $s->save();
        }

        $Types = [  
                    ['title'=>'Service','description'=>'Lorem ipsum2..'],
                    ['title'=>'Product','description'=>'Lorem ipsum3..']
                    ];

        foreach($Types as $type){
            $t = new ProductServicesTypes;
            $t->title = $type['title'];
            $t->description = $type['description'];
            $t->save();
        }


        $AccountingCode = [  
                        ['title'=>'B3000'],
                        ['title'=>'A2000'],
                        ['title'=>'A1000'],
                    ];

        foreach($AccountingCode as $code){
            $a = new AccountingDetails;
            $a->title = $code['title'];
            $a->save();
        }



    }
}
