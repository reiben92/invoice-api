<?php

use Illuminate\Database\Seeder;

use App\Models\Customers;
use App\Models\CustomerAddresses;
use App\Models\CustomerContacts;
use App\Models\CustomerDetails;
use App\Models\CustomerBalances;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$Customers = [
    				 	['customer_code'=>'C0000000001',
    				 		'customer_addresses'=>[
    				 							 'billing_address1'=>'A-2-01, CoPlace 2, 2260, Jalan Usahawan 1',
    				 							 'billing_city'=>'Cyberjaya',
    				 							 'billing_postcode'=>'63000',
    				 							 'billing_state'=>'Selangor',
    				 							 'billing_country'=>'Malaysia',

    				 		],
    				 		'customer_details'=>[
    				 							'phone_no'=>'+603 8686 8888',
    				 							'email'=>'sales@ixtelecom.net',
    				 							'fax'=>'+603 8686 8899',
    				 							'website'=>'http://www.ixtelecom.net',
                                                'customer_name'=>'IXTelecom Sdn Bhd','default_currency'=>1,
                                                'type'=>'Company',
    				 		],
    				 		'customer_contacts'=>[
    				 							'title'=>'Mr',
    				 							'first_name'=>'Noor Mohd Helmi',
    				 							'last_name'=>'Nong Hadzmi'
    				 		],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
    				 	],
    				 	['customer_code'=>'C0000000002',
    				 		'customer_addresses'=>[
    				 							 'billing_address1'=>'A-2-03, CoPlace 2, 2260, Jalan Usahawan 1',
    				 							 'billing_city'=>'Cyberjaya',
    				 							 'billing_postcode'=>'63000',
    				 							 'billing_state'=>'Selangor',
    				 							 'billing_country'=>'Malaysia',

    				 		],
    				 		'customer_details'=>[
                                                'customer_name'=>'IXGlobal Sdn Bhd','default_currency'=>1,
    				 							'phone_no'=>'+603 8686 8888',
    				 							'email'=>'sales@ixtelecom.net',
    				 							'fax'=>'+603 8686 8899',
    				 							'website'=>'http://ixglobal.net',
                                                'type'=>'Company',
    				 		],
    				 		'customer_contacts'=>[
    				 							'title'=>'Mr',
    				 							'first_name'=>'Noor Mohd Helmi',
    				 							'last_name'=>'Nong Hadzmi'
    				 		],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
    				 	],
    				 	['customer_code'=>'C0000000003',
    				 		'customer_addresses'=>[
    				 							 'billing_address1'=>'B-2-05, CoPlace 2, 2260, Jalan Usahawan 1',
    				 							 'billing_city'=>'Cyberjaya',
    				 							 'billing_postcode'=>'63000',
    				 							 'billing_state'=>'Selangor',
    				 							 'billing_country'=>'Malaysia',

    				 		],
    				 		'customer_details'=>[
                                                'customer_name'=>'Boneybone Ventures Sdn Bhd','default_currency'=>1,
    				 							'phone_no'=>'+603 8686 8866',
    				 							'email'=>'sales@boneybone.com',
    				 							'fax'=>'+603 8686 8899',
    				 							'website'=>'http://boneybone.com',
                                                'type'=>'Company',
    				 		],
    				 		'customer_contacts'=>[
    				 							'title'=>'Mr',
    				 							'first_name'=>'Mohd Amzari',
    				 							'last_name'=>'Tajudeen'
    				 		],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
    				 	],
    				 	['customer_code'=>'C0000000004',
    				 		'customer_addresses'=>[
    				 							 'billing_address1'=>'B-2-05, CoPlace 2, 2260, Jalan Usahawan 1',
    				 							 'billing_city'=>'Cyberjaya',
    				 							 'billing_postcode'=>'63000',
    				 							 'billing_state'=>'Selangor',
    				 							 'billing_country'=>'Malaysia',

    				 		],
    				 		'customer_details'=>[
                                                'customer_name'=>'Boneybone Studios Sdn Bhd','default_currency'=>1,
    				 							'phone_no'=>'+603 8686 8866',
    				 							'email'=>'sales@boneybone.com',
    				 							'fax'=>'+603 8686 8899',
    				 							'website'=>'http://boneybone.studio',
                                                'type'=>'Company',
    				 		],
    				 		'customer_contacts'=>[
    				 							'title'=>'Mr',
    				 							'first_name'=>'Mohd Afiq',
    				 							'last_name'=>'Mohd Salam'
    				 		],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
    				 	],
    				 	['customer_code'=>'C0000000005',
    				 		'customer_addresses'=>[
    				 							 'billing_address1'=>'Cyberview Garden Villas, Persiaran Multimedia, Cyber 7',
    				 							 'billing_city'=>'Cyberjaya',
    				 							 'billing_postcode'=>'63000',
    				 							 'billing_state'=>'Selangor',
    				 							 'billing_country'=>'Malaysia',

    				 		],
    				 		'customer_details'=>[
                                                'customer_name'=>'Allo Technologies Sdn Bhd','default_currency'=>1,
    				 							'phone_no'=>'1300 38 8000',
    				 							'email'=>'info@allomy',
    				 							'fax'=>'+603 8800 5221',
    				 							'website'=>'https://allo.my',
                                                'type'=>'Company',
    				 		],
    				 		'customer_contacts'=>[
    				 							'title'=>'Mr',
    				 							'first_name'=>'Rodzy',
    				 							'last_name'=>'',
    				 		],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
    				 	],
    				 	['customer_code'=>'C0000000006',
    				 		'customer_addresses'=>[
    				 							 'billing_address1'=>'Menara TM, Jalan Pantai Baharu',
    				 							 'billing_city'=>'Kuala Lumpur',
    				 							 'billing_postcode'=>'50672',
    				 							 'billing_state'=>'Wilayah Persekutuan Kuala Lumpur',
    				 							 'billing_country'=>'Malaysia',

    				 		],
    				 		'customer_details'=>[
                                                'customer_name'=>'Telekom Malaysia Bhd','default_currency'=>1,
    				 							'phone_no'=>'100',
    				 							'email'=>'info@allomy',
    				 							'website'=>'https://www.tm.com.my',
                                                'type'=>'Company',
    				 		],
    				 		'customer_contacts'=>[
    				 							'title'=>'Mr',
    				 							'first_name'=>'Noor Kamarul',
    				 							'last_name'=>'Anuar Nuruddin'
    				 		],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
    				 	],
    				 	['customer_code'=>'C0000000007',
    				 		'customer_addresses'=>[
    				 							 'billing_address1'=>'250, 11211, E Horse Race Course Rd ',
    				 							 'billing_city'=>'Yangon',
    				 							 'billing_postcode'=>'R5GC+7M',
    				 							 'billing_state'=>'Yangon',
    				 							 'billing_country'=>'Myanmar',

    				 		],
    				 		'customer_details'=>[
                                                'customer_name'=>'Myanmar SpeedNet Co., Ltd','default_currency'=>2,
    				 							'phone_no'=>'+95 9 264 800 880',
    				 							'email'=>'info@mmspeednet.info',
    				 							'website'=>'https://mmspeednet.info',
                                                'type'=>'Company',
    				 		],
    				 		'customer_contacts'=>[
    				 							'title'=>'Miss',
    				 							'first_name'=>'Hnin',
    				 							'last_name'=>'Lei Lei'
    				 		],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
    				 	],
                        ['customer_code'=>'C0000000008',
                            'customer_addresses'=>[
                                                 'billing_address1'=>'11, Jalan Duta U1/12C Glenhill Saujana, Seksyen U1',
                                                 'billing_city'=>'Shah Alam',
                                                 'billing_postcode'=>'40150',
                                                 'billing_state'=>'Selangor',
                                                 'billing_country'=>'Malaysia',

                            ],
                            'customer_details'=>[
                                                'customer_name'=>'Nor Emil Nor Badli Munawir','default_currency'=>1,
                                                'phone_no'=>'+6 018 2530162',
                                                'email'=>'noremil@ixtelecom.net',
                                                'website'=>'http://www.emilbadli.me',
                                                'type'=>'Individual',
                            ],
                            'customer_contacts'=>[
                                                'title'=>'Mr',
                                                'first_name'=>'Nor Emil',
                                                'last_name'=>'Nor Badli Munawir'
                            ],
                            'customer_balances'=>[
                                                'open_balance'=>'0.00',
                            ],
                        ],
    				 ];

    	

    	foreach($Customers as $customer){
    		$c = new Customers;
    		$c->customer_code = $customer['customer_code'];
    		$c->save();



			$keys1 = array_keys($customer['customer_addresses']);
			$c1 = new CustomerAddresses;
			foreach($keys1 as $key){
				$c1->$key = $customer['customer_addresses'][$key];
			}	

			$c1->customer_id = $c->id;
			$c1->save();
    		

			$keys2 = array_keys($customer['customer_details']);
			$c2 = new CustomerDetails;

			foreach($keys2 as $key){
				$c2->$key = $customer['customer_details'][$key];
			}	

			$c2->customer_id = $c->id;
			$c2->save();

			$keys3 = array_keys($customer['customer_contacts']);
			$c3 = new CustomerContacts;

			foreach($keys3 as $key){
				$c3->$key = $customer['customer_contacts'][$key];
			}	

			$c3->customer_id = $c->id;
			$c3->save();

            $keys4 = array_keys($customer['customer_balances']);
            $c4 = new CustomerBalances;

            foreach($keys4 as $key){
                $c4->$key = $customer['customer_balances'][$key];
            }   

            $c4->customer_id = $c->id;
            $c4->save();
    	}
    }
}
