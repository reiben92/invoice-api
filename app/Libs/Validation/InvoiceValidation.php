<?php

namespace App\Libs\Validation;


class InvoiceValidation
{   
    protected $data=[];
    protected $code=200;
    protected $err_msg=[];

    public function __construct(array $data){
        $this->data = $data;
    }


    public function validate(){
        $this->checkCustomer();
        $this->checkInvoiceItems();
        $this->checkInvoices();
        $this->checkInvoiceSummary();
        $this->checkInvoiceRecurring();

        if(sizeof($this->err_msg)>0){
            $this->code = 500;
        }

        return ['code'=>$this->code,'err_msg'=>$this->err_msg];
    }

    protected function checkCustomer(){
        if(empty($this->data['invoice_customer'])){
            array_push($this->err_msg,'Customer details not found in request.'); 
        }
        else{
            if(empty($this->data['invoice_customer']['customer_id'])){
                array_push($this->err_msg,'Empty customer data. Please select a customer.'); 
            }
        }
    }

    protected function checkInvoices(){
        if(empty($this->data['invoice_details'])){
            array_push($this->err_msg,'Invoice details not found in request.'); 
        }
        else{
            $this->checkInvoiceDates();
        }
    }

    protected function checkInvoiceSummary(){
        if(empty($this->data['invoice_summary'])){
            array_push($this->err_msg,'Invoice summary not found in request.'); 
        }
        else{
            if(empty($this->data['invoice_summary']['currency_id'])){
                array_push($this->err_msg,'Missing currency. Please select a currency.'); 
            }
        }
    }

    protected function checkInvoiceItems(){
        if(empty($this->data['invoice_items'])){
            array_push($this->err_msg,'Invoice items not found in request.'); 
        }
        else{
            if(sizeof($this->data['invoice_items'])==0){
                array_push($this->err_msg,'Empty invoice items. Please input invoice items.'); 
            }
        }
    }

    protected function checkInvoiceDates(){
        if(empty($this->data['invoice_details']['invoice_date'])){
            array_push($this->err_msg,'Empty date. Please input invoice dates.'); 
        }

        if(empty($this->data['invoice_details']['payment_due'])){
            array_push($this->err_msg,'Empty date. Please input payment due for invoice.'); 
        }
    }

    protected function checkInvoiceRecurring(){
        if(!empty($this->data['invoice_recurring'])){
            if(!empty($this->data['invoice_recurring']['interval'])){
                $this->checkInvoiceRecurringValues();
            }
        }
    }

    protected function checkInvoiceRecurringValues(){
        if(empty($this->data['invoice_recurring']['interval_value'])){
            array_push($this->err_msg,'Empty interval value. Please input every interval for recurring invoice.'); 
        }

        if(empty($this->data['invoice_recurring']['payment_terms'])){
            array_push($this->err_msg,'Empty payment terms. Please input payment terms for recurring invoice.'); 
        }

        if(empty($this->data['invoice_recurring']['start_date'])){
            array_push($this->err_msg,'Empty start date. Please input start date for recurring invoice.'); 
        }

        if(empty($this->data['invoice_recurring']['end_date'])){
            array_push($this->err_msg,'Empty end date. Please input end date for recurring invoice.'); 
        }       
    }
}
