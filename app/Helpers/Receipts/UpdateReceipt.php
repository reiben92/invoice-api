<?php

namespace App\Helpers\Receipts;

use App\Models\Receipts;
use App\Models\ReceiptItems;
use App\Models\ReceiptTypes;
use App\Models\ReceiptStatuses;
use App\Models\ReceiptCurrency;
use App\Models\ReceiptDetails;
use App\Models\ReceiptItemsTaxes;
use App\Models\ReceiptStatementOfAccount;
use App\Models\ReceiptCustomers;
use App\Models\ReceiptPurchaseOrder;
use App\Models\ReceiptQuotation;
use App\Models\ReceiptSummary;

class UpdateReceipt
{   
    protected $data=[];
    protected $Receipts=[];
    protected $Receipt_items=[];
    protected $Receipt_summary=[];
    protected $Receipt_details=[];
    protected $Receipt_customer=[];

    protected $receipt_items_keys = ['amount','quantity','rate'];
    protected $receipt_summary_keys = ['total_tax','grand_total','sub_total','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->update();
        $this->updateItems();
        $this->updateCustomer();
        $this->updateDetails();
        $this->updateSummary();
        $this->deleteReceiptItems();
    }

    public function update(){
        $this->Receipts = Receipts::find($this->data['receipt']['id']);
        $this->Receipts->receipt_no = $this->data['receipt']['receipt_no'];

        if(empty($this->data['receipt']['status_id'])){
            $this->Receipts->receipt_status_id = 1;
        }

        $this->Receipts->save();
    }

    public function updateItems(){
        foreach($this->data['receipt_items'] as $item){
            if($item['id']=='NEW'){
                $receipt_items = new ReceiptItems;
            }
            else{
                $receipt_items = ReceiptItems::find($item['id']);
            }

            foreach($this->receipt_items_keys as $key){
                $receipt_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($receipt_items->$key)){
                    $receipt_items->$key = 0.00;
                }
            }

            $receipt_items->description = $item['description'];
            if(!empty($item['receipt_type_id'])){
                $receipt_items->receipt_type_id = $item['receipt_type_id'];
            }
            else{
                $receipt_items->receipt_type_id = null;
            }

            if(!empty($item['product_id'])){
                $receipt_items->product_id = $item['product_id'];
            }
            else{
                $receipt_items->product_id = null;
            }

            if(!empty($item['invoice_id'])){
                $receipt_items->invoice_id = $item['invoice_id'];
            }
            else{
                $receipt_items->invoice_id = null;
            }

            $receipt_items->receipt_id = $this->Receipts->id;
            $receipt_items->save();

            $receipt_item_taxes = [];

            if(!empty($item['tax'])){
                if(!empty($item['tax']['id'])){
                    $receipt_item_taxes = ReceiptItemsTaxes::find($item['tax']['id']);

                    if(empty($item['tax']['tax_id'])){
                        if(!empty($receipt_item_taxes)){
                            $receipt_item_taxes->delete();
                        }
                    }
                    else{
                        if(!empty($receipt_item_taxes)){
                            $receipt_item_taxes = $this->updateReceiptTaxItem($receipt_item_taxes,$item,$receipt_items);
                        }
                    }
                }
                else{
                    $receipt_item_taxes = new ReceiptItemsTaxes;
                    $receipt_item_taxes = $this->updateReceiptTaxItem($receipt_item_taxes,$item,$receipt_items);
                }

                $receipt_items['receipt_items_taxes'] = $receipt_item_taxes;
            }
           
            array_push($this->Receipt_items,$receipt_items);
        }
    }

    public function updateReceiptTaxItem($objs,$item,$receipt_items){
        $objs->tax_type_id = $item['tax']['tax_id'];
        $objs->tax_rate = $item['tax']['default_tax_rate'];
        $objs->receipt_item_id = $receipt_items->id;
        $objs->receipt_id = $this->Receipts->id;


        $objs->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
        if(empty($objs->amount)){
            $objs->amount = 0.00;
        }

        $objs->save();
        return $objs;
    }

    public function updateCustomer(){
        $this->Receipt_customer = ReceiptCustomers::find($this->data['receipt_customer']['id']);
        if(!empty($this->Receipt_customer)){
            $this->Receipt_customer->customer_id = $this->data['receipt_customer']['customer_id'];
            $this->Receipt_customer->receipt_id = $this->Receipts->id;
            $this->Receipt_customer->save();
        }
    }

    public function updateDetails(){
        $this->Receipt_details = ReceiptDetails::find($this->data['receipt_details']['id']);
        if(!empty($this->Receipt_details)){
            $this->Receipt_details->billing_address1 = $this->data['receipt_details']['billing_address_line1'];
            $this->Receipt_details->billing_address2 = $this->data['receipt_details']['billing_address_line2'];
            $this->Receipt_details->billing_address3 = $this->data['receipt_details']['billing_address_line3'];
            $this->Receipt_details->billing_city = $this->data['receipt_details']['billing_city'];
            $this->Receipt_details->billing_postcode = $this->data['receipt_details']['billing_postcode'];
            $this->Receipt_details->billing_state = $this->data['receipt_details']['billing_state'];
            $this->Receipt_details->billing_country = $this->data['receipt_details']['billing_country'];
            
            if(!empty($this->data['receipt_details']['receipt_date'])){
                $this->Receipt_details->receipt_date = $this->data['receipt_details']['receipt_date'];
            }

            if(!empty($this->data['receipt_details']['receipt_description'])){
                $this->Receipt_details->receipt_description = $this->data['receipt_details']['receipt_description'];
            }

            $this->Receipt_details->billing_email = $this->data['receipt_details']['billing_email'];
            $this->Receipt_details->receipt_id = $this->Receipts->id;
            $this->receipt_details->payment_method_id = $this->data['receipt_details']['payment_method_id'];
            $this->receipt_details->bank_account_id = $this->data['receipt_details']['bank_account_id'];
            $this->Receipt_details->save();
        }
    }

    public function updateSummary(){
        $this->Receipt_summary = ReceiptSummary::find($this->data['receipt_summary']['id']);
        if(!empty($this->Receipt_summary)){
            $this->Receipt_summary->receipt_id = $this->Receipts->id;

            foreach($this->receipt_summary_keys as $key){
                $this->Receipt_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['receipt_summary'][$key]);
                if(empty($this->Receipt_summary->$key)){
                    $this->Receipt_summary->$key = 0.00;
                }
            }
            
            $this->Receipt_summary->save();
        }
    }

    public function deleteReceiptItems(){
        foreach($this->data['deleted_receipt_items'] as $deleted_receipt_items){
            $receipt_item_deleted = ReceiptItems::find($deleted_receipt_items['id']);

            if(!empty($receipt_item_deleted)){
                $receipt_item_deleted->delete();

                $receipt_item_deleted_tax = ReceiptItemsTaxes::find($deleted_receipt_items['tax']['id']);

                if(!empty($receipt_item_deleted_tax)){
                    $receipt_item_deleted_tax->delete();
                }
            }
        }
    }
}
