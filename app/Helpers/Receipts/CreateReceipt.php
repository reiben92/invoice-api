<?php

namespace App\Helpers\Receipts;

use App\Models\Receipts;
use App\Models\ReceiptItems;
use App\Models\ReceiptTypes;
use App\Models\ReceiptStatuses;
use App\Models\ReceiptCurrency;
use App\Models\ReceiptDetails;
use App\Models\ReceiptItemsTaxes;
use App\Models\ReceiptStatementOfAccount;
use App\Models\ReceiptCustomers;
use App\Models\ReceiptPurchaseOrder;
use App\Models\ReceiptQuotation;
use App\Models\ReceiptSummary;

use App\Helpers\Counters\CreateRunningNumber;

class CreateReceipt
{   
    protected $data=[];
    protected $receipts=[];
    protected $receipt_items=[];
    protected $receipt_summary=[];
    protected $receipt_details=[];
    protected $receipt_customer=[];

    protected $receipt_items_keys = ['amount','quantity','rate'];
    protected $receipt_summary_keys = ['total_tax','grand_total','sub_total','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->create();
        $this->createItems();
        $this->createCustomer();
        $this->createDetails();
        $this->createSummary();

        return $this->receipts;
    }

    public function create(){
        $running_number = new CreateRunningNumber('Receipt','R');

        $this->receipts = new Receipts;
        $this->receipts->receipt_no = $running_number->generateReceiptNo();

        if(empty($this->data['receipt']['status_id'])){
            $this->receipts->receipt_status_id = 1;
        }

        $this->receipts->save();
    }

    public function createItems(){
        foreach($this->data['receipt_items'] as $item){
            $receipt_items = new ReceiptItems;

            foreach($this->receipt_items_keys as $key){
                $receipt_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($receipt_items->$key)){
                    $receipt_items->$key = 0.00;
                }
            }

            $receipt_items->description = $item['description'];
            if(!empty($item['receipt_type_id'])){
                $receipt_items->receipt_type_id = $item['receipt_type_id'];
            }

            if(!empty($item['product_id'])){
                $receipt_items->product_id = $item['product_id'];
            }

            $receipt_items->receipt_id = $this->receipts->id;
            if(!empty($item['invoice_id'])){
                $receipt_items->invoice_id = $item['invoice_id'];
            }
            
            $receipt_items->save();

            if(!empty($item['tax'])){
                if(!empty($item['tax']['tax_id'])){
                    $receipt_item_taxes = new ReceiptItemsTaxes;
                    $receipt_item_taxes->tax_type_id = $item['tax']['tax_id'];
                    $receipt_item_taxes->tax_rate = $item['tax']['default_tax_rate'];
                    $receipt_item_taxes->receipt_item_id = $receipt_items->id;
                    $receipt_item_taxes->receipt_id = $this->receipts->id;

                    $receipt_item_taxes->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
                    if(empty($receipt_item_taxes->amount)){
                        $receipt_item_taxes->amount = 0.00;
                    }

                    $receipt_item_taxes->save();

                    $receipt_items['receipt_items_taxes'] = $receipt_item_taxes;
                }
            }

           
            array_push($this->receipt_items,$receipt_items);
        }
    }

    public function createCustomer(){
        $this->receipt_customer = new ReceiptCustomers;
        $this->receipt_customer->customer_id = $this->data['receipt_customer']['customer_id'];
        $this->receipt_customer->receipt_id = $this->receipts->id;
        $this->receipt_customer->save();
    }

    public function createDetails(){
        $this->receipt_details = new ReceiptDetails;
        $this->receipt_details->billing_address1 = $this->data['receipt_details']['billing_address_line1'];
        $this->receipt_details->billing_address2 = $this->data['receipt_details']['billing_address_line2'];
        $this->receipt_details->billing_address3 = $this->data['receipt_details']['billing_address_line3'];
        $this->receipt_details->billing_city = $this->data['receipt_details']['billing_city'];
        $this->receipt_details->billing_postcode = $this->data['receipt_details']['billing_postcode'];
        $this->receipt_details->billing_state = $this->data['receipt_details']['billing_state'];
        $this->receipt_details->billing_country = $this->data['receipt_details']['billing_country'];
        $this->receipt_details->receipt_date = $this->data['receipt_details']['receipt_date'];
        $this->receipt_details->billing_email = $this->data['receipt_details']['billing_email'];
        $this->receipt_details->receipt_description = $this->data['receipt_details']['receipt_description'];
        $this->receipt_details->payment_method_id = $this->data['receipt_details']['payment_method_id'];
        $this->receipt_details->bank_account_id = $this->data['receipt_details']['bank_account_id'];
        $this->receipt_details->receipt_id = $this->receipts->id;
        $this->receipt_details->save();
    }

    public function createSummary(){
        $this->receipt_summary = new ReceiptSummary;
        $this->receipt_summary->receipt_id = $this->receipts->id;

        foreach($this->receipt_summary_keys as $key){
            $this->receipt_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['receipt_summary'][$key]);
            if(empty($this->receipt_summary->$key)){
                $this->receipt_summary->$key = 0.00;
            }
        }

        $this->receipt_summary->save();
    }

    protected function checkReceiptNo(){
        $receipt_no = $this->counter->prefix.str_pad($this->max_invoice, $this->counter->pre, "0", STR_PAD_LEFT);
        $count = Receipts::where('receipt_no',$receipt_no)->count();
        if($count==0){
            return $receipt_no;
        }
        else{
            $this->max_receipt++;
            return $this->checkInvoiceNo();
        }
    }
}
