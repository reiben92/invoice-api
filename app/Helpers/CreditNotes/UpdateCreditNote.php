<?php

namespace App\Helpers\CreditNotes;

use App\Models\CreditNotes;
use App\Models\CreditNoteItems;
use App\Models\CreditNoteTypes;
use App\Models\CreditNoteStatuses;
use App\Models\CreditNoteCurrency;  
use App\Models\CreditNoteDetails;
use App\Models\CreditNoteItemsTaxes;
use App\Models\CreditNoteCustomers;
use App\Models\CreditNoteSummary;

class UpdateCreditNote
{   
    protected $data=[];
    protected $credit_notes=[];
    protected $credit_note_items=[];
    protected $credit_note_summary=[];
    protected $credit_note_details=[];
    protected $credit_note_customer=[];

    protected $credit_note_items_keys = ['amount','discount','quantity','rate'];
    protected $credit_note_summary_keys = ['total_tax','grand_total','total_discount','sub_total','discount_amount','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->update();
        $this->updateItems();
        $this->updateCustomer();
        $this->updateDetails();
        $this->updateSummary();
        $this->deleteCreditNoteItems();
    }

    public function update(){
        $this->credit_notes = CreditNotes::find($this->data['credit_note']['id']);

        if(empty($this->data['credit_note']['status_id'])){
            $this->credit_notes->credit_note_status_id = 1;
        }

        $this->credit_notes->save();
    }

    public function updateItems(){
        foreach($this->data['credit_note_items'] as $item){
            if($item['id']=='NEW'){
                $credit_note_items = new CreditNoteItems;
            }
            else{
                $credit_note_items = CreditNoteItems::find($item['id']);
            }

            foreach($this->credit_note_items_keys as $key){
                $credit_note_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($credit_note_items->$key)){
                    $credit_note_items->$key = 0.00;
                }
            }

            $credit_note_items->description = $item['description'];
            $credit_note_type = null;
            $credit_note_type_id = null;

            if(!empty($item['credit_note_type'])){
                if(!empty($item['credit_note_type']['type'])){
                    $credit_note_type = $item['credit_note_type']['type'];
                }

                if(!empty($item['credit_note_type']['type_id'])){
                    $credit_note_type_id = $item['credit_note_type']['type_id'];
                }
            }

            if(!empty($item['product_id'])){
                $credit_note_items->product_id = $item['product_id'];
            }

            $credit_note_items->credit_note_type = $credit_note_type;
            $credit_note_items->credit_note_type_id = $credit_note_type_id;

            $credit_note_items->credit_note_id = $this->credit_notes->id;
            $credit_note_items->save();

            $credit_note_item_taxes = [];

            if(!empty($item['tax'])){
                if(!empty($item['tax']['id'])){
                    $credit_note_item_taxes = CreditNoteItemsTaxes::find($item['tax']['id']);

                    if(empty($item['tax']['tax_id'])){
                        if(!empty($credit_note_item_taxes)){
                            $credit_note_item_taxes->delete();
                        }
                    }
                    else{
                        if(!empty($credit_note_item_taxes)){
                            $credit_note_item_taxes = $this->updateCreditNoteTaxItem($credit_note_item_taxes,$item,$credit_note_items);
                        }
                    }
                }
                else{
                    $credit_note_item_taxes = new CreditNoteItemsTaxes;
                    $credit_note_item_taxes = $this->updateCreditNoteTaxItem($credit_note_item_taxes,$item,$credit_note_items);
                }

                $credit_note_items['credit_note_items_taxes'] = $credit_note_item_taxes;
            }
           
            array_push($this->credit_note_items,$credit_note_items);
        }
    }

    public function updateCreditNoteTaxItem($objs,$item,$credit_note_items){
        $objs->tax_type_id = $item['tax']['tax_id'];
        $objs->tax_rate = $item['tax']['default_tax_rate'];
        $objs->credit_note_item_id = $credit_note_items->id;
        $objs->credit_note_id = $this->credit_notes->id;

        $objs->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
        if(empty($objs->amount)){
            $objs->amount = 0.00;
        }

        $objs->save();
        return $objs;
    }

    public function updateCustomer(){
        $this->credit_note_customer = CreditNoteCustomers::find($this->data['credit_note_customer']['id']);
        if(!empty($this->credit_note_customer)){
            $this->credit_note_customer->customer_id = $this->data['credit_note_customer']['customer_id'];
            $this->credit_note_customer->credit_note_id = $this->credit_notes->id;
            $this->credit_note_customer->save();
        }
    }

    public function updateDetails(){
        $this->credit_note_details = CreditNoteDetails::find($this->data['credit_note_details']['id']);
        if(!empty($this->credit_note_details)){
            $this->credit_note_details->billing_address1 = $this->data['credit_note_details']['billing_address_line1'];
            $this->credit_note_details->billing_address2 = $this->data['credit_note_details']['billing_address_line2'];
            $this->credit_note_details->billing_address3 = $this->data['credit_note_details']['billing_address_line3'];
            $this->credit_note_details->billing_city = $this->data['credit_note_details']['billing_city'];
            $this->credit_note_details->billing_postcode = $this->data['credit_note_details']['billing_postcode'];
            $this->credit_note_details->billing_state = $this->data['credit_note_details']['billing_state'];
            $this->credit_note_details->billing_country = $this->data['credit_note_details']['billing_country'];
            
            if(!empty($this->data['credit_note_details']['credit_note_date'])){
                $this->credit_note_details->credit_note_date = $this->data['credit_note_details']['credit_note_date'];
            }

            if(!empty($this->data['credit_note_details']['credit_note_description'])){
                $this->credit_note_details->credit_note_description = $this->data['credit_note_details']['credit_note_description'];
            }

            $this->credit_note_details->billing_email = $this->data['credit_note_details']['billing_email'];
            $this->credit_note_details->credit_note_id = $this->credit_notes->id;
            $this->credit_note_details->save();
        }
    }

    public function updateSummary(){
        $this->credit_note_summary = CreditNoteSummary::find($this->data['credit_note_summary']['id']);
        if(!empty($this->credit_note_summary)){
            $this->credit_note_summary->credit_note_id = $this->credit_notes->id;

            foreach($this->credit_note_summary_keys as $key){
                $this->credit_note_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['credit_note_summary'][$key]);
                if(empty($this->credit_note_summary->$key)){
                    $this->credit_note_summary->$key = 0.00;
                }
            }

            $this->credit_note_summary->discount_type = $this->data['credit_note_summary']['discount_type'];
            $this->credit_note_summary->save();
        }
    }

    public function deleteCreditNoteItems(){
        foreach($this->data['deleted_credit_note_items'] as $deleted_credit_note_items){
            $credit_note_item_deleted = CreditNoteItems::find($deleted_credit_note_items['id']);

            if(!empty($credit_note_item_deleted)){
                $credit_note_item_deleted->delete();

                $credit_note_item_deleted_tax = CreditNoteItemsTaxes::find($deleted_credit_note_items['tax']['id']);

                if(!empty($credit_note_item_deleted_tax)){
                    $credit_note_item_deleted_tax->delete();
                }
            }
        }
    }
}
