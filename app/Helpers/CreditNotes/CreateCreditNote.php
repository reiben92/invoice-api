<?php

namespace App\Helpers\CreditNotes;

use App\Models\CreditNotes;
use App\Models\CreditNoteItems;
use App\Models\CreditNoteTypes;
use App\Models\CreditNoteStatuses;
use App\Models\CreditNoteCurrency;
use App\Models\CreditNoteDetails;
use App\Models\CreditNoteItemsTaxes;
use App\Models\CreditNoteCustomers;
use App\Models\CreditNoteSummary;

use App\Helpers\Counters\CreateRunningNumber;

class CreateCreditNote
{   
    protected $data=[];
    protected $credit_notes=[];
    protected $credit_note_items=[];
    protected $credit_note_purchase_order=[];
    protected $credit_note_quotation=[];
    protected $credit_note_summary=[];
    protected $credit_note_details=[];
    protected $credit_note_customer=[];

    protected $credit_note_items_keys = ['amount','discount','quantity','rate'];
    protected $credit_note_summary_keys = ['total_tax','grand_total','total_discount','sub_total','discount_amount','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->create();
        $this->createItems();
        $this->createCustomer();
        $this->createDetails();
        $this->createSummary();

        return $this->credit_notes;
    }

    public function create(){
        
        $running_number = new CreateRunningNumber('Credit Note','CR');

        $this->credit_notes = new CreditNotes;
        $this->credit_notes->credit_note_no = $running_number->generateCreditNoteNo();

        if(empty($this->data['credit_note']['status_id'])){
            $this->credit_notes->credit_note_status_id = 1;
        }

        $this->credit_notes->save();
    }

    public function createItems(){
        foreach($this->data['credit_note_items'] as $item){
            $credit_note_items = new CreditNoteItems;

            foreach($this->credit_note_items_keys as $key){
                $credit_note_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($credit_note_items->$key)){
                    $credit_note_items->$key = 0.00;
                }
            }

            $credit_note_items->description = $item['description'];
            $credit_note_type = null;
            $credit_note_type_id = null;

            if(!empty($item['credit_note_type'])){
                if(!empty($item['credit_note_type']['type'])){
                    $credit_note_type = $item['credit_note_type']['type'];
                }

                if(!empty($item['credit_note_type']['type_id'])){
                    $credit_note_type_id = $item['credit_note_type']['type_id'];
                }
            }

            $credit_note_items->credit_note_type = $credit_note_type;
            $credit_note_items->credit_note_type_id = $credit_note_type_id;

            if(!empty($item['product_id'])){
                $credit_note_items->product_id = $item['product_id'];
            }

            $credit_note_items->credit_note_id = $this->credit_notes->id;
            $credit_note_items->save();

            if(!empty($item['tax'])){
                if(!empty($item['tax']['tax_id'])){
                    $credit_note_item_taxes = new CreditNoteItemsTaxes;
                    $credit_note_item_taxes->tax_type_id = $item['tax']['tax_id'];
                    $credit_note_item_taxes->tax_rate = $item['tax']['default_tax_rate'];
                    $credit_note_item_taxes->credit_note_item_id = $credit_note_items->id;
                    $credit_note_item_taxes->credit_note_id = $this->credit_notes->id;

                    $credit_note_item_taxes->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
                    if(empty($credit_note_item_taxes->amount)){
                        $credit_note_item_taxes->amount = 0.00;
                    }

                    $credit_note_item_taxes->save();

                    $credit_note_items['credit_note_items_taxes'] = $credit_note_item_taxes;
                }
            }

            array_push($this->credit_note_items,$credit_note_items);
        }
    }

    public function createCustomer(){
        $this->credit_note_customer = new CreditNoteCustomers;
        $this->credit_note_customer->customer_id = $this->data['credit_note_customer']['customer_id'];
        $this->credit_note_customer->credit_note_id = $this->credit_notes->id;
        $this->credit_note_customer->save();
    }

    public function createDetails(){
        $this->credit_note_details = new CreditNoteDetails;
        $this->credit_note_details->billing_address1 = $this->data['credit_note_details']['billing_address_line1'];
        $this->credit_note_details->billing_address2 = $this->data['credit_note_details']['billing_address_line2'];
        $this->credit_note_details->billing_address3 = $this->data['credit_note_details']['billing_address_line3'];
        $this->credit_note_details->billing_city = $this->data['credit_note_details']['billing_city'];
        $this->credit_note_details->billing_postcode = $this->data['credit_note_details']['billing_postcode'];
        $this->credit_note_details->billing_state = $this->data['credit_note_details']['billing_state'];
        $this->credit_note_details->billing_country = $this->data['credit_note_details']['billing_country'];
        if(!empty($this->data['credit_note_details']['credit_note_date'])){
            $this->credit_note_details->credit_note_date = $this->data['credit_note_details']['credit_note_date'];
        }

        if(!empty($this->data['credit_note_details']['credit_note_description'])){
            $this->credit_note_details->credit_note_description = $this->data['credit_note_details']['credit_note_description'];
        }
        $this->credit_note_details->billing_email = $this->data['credit_note_details']['billing_email'];
        $this->credit_note_details->credit_note_id = $this->credit_notes->id;
        $this->credit_note_details->save();
    }


    public function createSummary(){
        $this->credit_note_summary = new CreditNoteSummary;
        $this->credit_note_summary->credit_note_id = $this->credit_notes->id;

        foreach($this->credit_note_summary_keys as $key){
            $this->credit_note_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['credit_note_summary'][$key]);
            if(empty($this->credit_note_summary->$key)){
                $this->credit_note_summary->$key = 0.00;
            }
        }

        $this->credit_note_summary->discount_type = $this->data['credit_note_summary']['discount_type'];
        $this->credit_note_summary->save();
    }
}
