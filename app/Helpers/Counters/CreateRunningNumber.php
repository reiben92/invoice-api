<?php

namespace App\Helpers\Counters;

use App\Models\Counters;
use App\Models\Invoices;
use App\Models\Receipts;
use App\Models\CreditNotes;

class CreateRunningNumber
{   
    protected $max_number;
    protected $counter;
    protected $type;
    protected $prefix;
    
    public $running_number;


    public function __construct($type,$prefix){
        $this->type = $type;
        $this->prefix = $prefix;

        $this->counter = Counters::where('type',$this->type)->where('prefix',$this->prefix)->first(['id','value','prefix','pre']);

        if(empty($this->counter)){
            $this->counter = new Counters;
            $this->counter->type=$this->type;
            $this->counter->value=0;
            $this->counter->prefix=$this->prefix;
            $this->counter->pre=10;
            $this->counter->save();
        }

        $this->max_number = $this->counter->value+1;
    }

    public function generateInvoiceNo(){
        $running_number = $this->counter->prefix.str_pad($this->max_number, $this->counter->pre, "0", STR_PAD_LEFT);
        $count = Invoices::where('invoice_no',$running_number)->count();
        if($count==0){
            $this->counter->value = $this->max_number;
            $this->counter->save();

            return $running_number;
        }
        else{
            $this->max_number++;
            return $this->generateInvoiceNo();
        }
    }

    public function generateReceiptNo(){
        $running_number = $this->counter->prefix.str_pad($this->max_number, $this->counter->pre, "0", STR_PAD_LEFT);
        $count = Receipts::where('receipt_no',$running_number)->count();
        if($count==0){
            $this->counter->value = $this->max_number;
            $this->counter->save();
            
            return $running_number;
        }
        else{
            $this->max_number++;
            return $this->generateReceiptNo();
        }
    }

    public function generateCreditNoteNo(){
        $running_number = $this->counter->prefix.str_pad($this->max_number, $this->counter->pre, "0", STR_PAD_LEFT);
        $count = CreditNotes::where('credit_note_no',$running_number)->count();
        if($count==0){
            $this->counter->value = $this->max_number;
            $this->counter->save();
            
            return $running_number;
        }
        else{
            $this->max_number++;
            return $this->generateCreditNoteNo();
        }
    }
}
