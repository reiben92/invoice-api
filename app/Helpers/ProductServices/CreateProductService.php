<?php

namespace App\Helpers\ProductServices;

use App\Models\ProductServices;
use App\Models\ProductServicesDetails;
use App\Models\ProductServicesAccounting;

class CreateProductService
{   
    protected $data=[];
    protected $product_services=[];
    protected $product_services_details=[];
    protected $product_services_accounting=[];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->create();
        $this->createDetails();
        $this->createAccountingDetails();

        return $this->product_services;
    }

    public function create(){
        $this->product_services = new ProductServices;
        $this->product_services->status_id = 1;
        $this->product_services->save();
    }

    public function createDetails(){
        $this->product_services_details = new ProductServicesDetails;
        $this->product_services_details->product_id = $this->product_services->id;
        $this->product_services_details->product_name = $this->data['product_services_details']['product_name'];
        $this->product_services_details->product_description = $this->data['product_services_details']['product_description'];
        $this->product_services_details->product_type_id = $this->data['product_services_details']['product_type_id'];
        $this->product_services_details->product_code = ProductServices::max('id')+1;
        $this->product_services_details->save();
    }

    public function createAccountingDetails(){
        $this->product_services_accounting = new ProductServicesAccounting;
        $this->product_services_accounting->product_id = $this->product_services->id;
        $this->product_services_accounting->product_detail_id = $this->product_services_details->id;
        $this->product_services_accounting->accounting_id = $this->data['product_services_accounting']['accounting_id'];
        $this->product_services_accounting->save();
    }
}
