<?php

namespace App\Helpers\DebitNotes;

use App\Models\DebitNotes;
use App\Models\DebitNoteItems;
use App\Models\DebitNoteTypes;
use App\Models\DebitNoteStatuses;
use App\Models\DebitNoteCurrency;
use App\Models\DebitNoteDetails;
use App\Models\DebitNoteItemsTaxes;
use App\Models\DebitNotesCustomer;
use App\Models\DebitNoteSummary;

class UpdateDebitNote
{   
    protected $data=[];
    protected $debit_notes=[];
    protected $debit_note_items=[];
    protected $debit_note_summary=[];
    protected $debit_note_details=[];
    protected $debit_note_customer=[];

    protected $debit_note_items_keys = ['amount','discount','quantity','rate'];
    protected $debit_note_summary_keys = ['total_tax','grand_total','total_discount','sub_total','discount_amount','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->update();
        $this->updateItems();
        $this->updateCustomer();
        $this->updateDetails();
        $this->updateSummary();
        $this->deleteDebitNoteItems();
    }

    public function update(){
        $this->debit_notes = DebitNotes::find($this->data['debit_note']['id']);

        if(empty($this->data['debit_note']['status_id'])){
            $this->debit_notes->debit_note_status_id = 1;
        }

        $this->debit_notes->save();
    }

    public function updateItems(){
        foreach($this->data['debit_note_items'] as $item){
            if($item['id']=='NEW'){
                $debit_note_items = new DebitNoteItems;
            }
            else{
                $debit_note_items = DebitNoteItems::find($item['id']);
            }

            foreach($this->debit_note_items_keys as $key){
                $debit_note_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($debit_note_items->$key)){
                    $debit_note_items->$key = 0.00;
                }
            }

            $debit_note_items->description = $item['description'];
            if(!empty($item['debit_note_type_id'])){
                $debit_note_items->debit_note_type_id = $item['debit_note_type_id'];
            }

            if(!empty($item['product_id'])){
                $debit_note_items->product_id = $item['product_id'];
            }

            $debit_note_items->debit_note_id = $this->debit_notes->id;
            $debit_note_items->save();

            $debit_note_item_taxes = [];

            if(!empty($item['tax'])){
                if(!empty($item['tax']['id'])){
                    $debit_note_item_taxes = DebitNoteItemsTaxes::find($item['tax']['id']);

                    if(empty($item['tax']['tax_id'])){
                        if(!empty($debit_note_item_taxes)){
                            $debit_note_item_taxes->delete();
                        }
                    }
                    else{
                        if(!empty($debit_note_item_taxes)){
                            $debit_note_item_taxes = $this->updateDebitNoteTaxItem($debit_note_item_taxes,$item,$debit_note_items);
                        }
                    }
                }
                else{
                    $debit_note_item_taxes = new DebitNoteItemsTaxes;
                    $debit_note_item_taxes = $this->updateDebitNoteTaxItem($debit_note_item_taxes,$item,$debit_note_items);
                }

                $debit_note_items['debit_note_items_taxes'] = $debit_note_item_taxes;
            }
           
            array_push($this->debit_note_items,$debit_note_items);
        }
    }

    public function updateDebitNoteTaxItem($objs,$item,$debit_note_items){
        $objs->tax_type_id = $item['tax']['tax_id'];
        $objs->tax_rate = $item['tax']['default_tax_rate'];
        $objs->debit_note_item_id = $debit_note_items->id;
        $objs->debit_note_id = $this->debit_notes->id;

        $objs->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
        if(empty($objs->amount)){
            $objs->amount = 0.00;
        }

        $objs->save();
        return $objs;
    }

    public function updateCustomer(){
        $this->debit_note_customer = DebitNotesCustomer::find($this->data['debit_note_customer']['id']);
        if(!empty($this->debit_note_customer)){
            $this->debit_note_customer->customer_id = $this->data['debit_note_customer']['customer_id'];
            $this->debit_note_customer->debit_note_id = $this->debit_notes->id;
            $this->debit_note_customer->save();
        }
    }

    public function updateDetails(){
        $this->debit_note_details = DebitNoteDetails::find($this->data['debit_note_details']['id']);
        if(!empty($this->debit_note_details)){
            $this->debit_note_details->billing_address1 = $this->data['debit_note_details']['billing_address_line1'];
            $this->debit_note_details->billing_address2 = $this->data['debit_note_details']['billing_address_line2'];
            $this->debit_note_details->billing_address3 = $this->data['debit_note_details']['billing_address_line3'];
            $this->debit_note_details->billing_city = $this->data['debit_note_details']['billing_city'];
            $this->debit_note_details->billing_postcode = $this->data['debit_note_details']['billing_postcode'];
            $this->debit_note_details->billing_state = $this->data['debit_note_details']['billing_state'];
            $this->debit_note_details->billing_country = $this->data['debit_note_details']['billing_country'];
            if(!empty($this->data['debit_note_details']['debit_note_date'])){
                $this->debit_note_details->debit_note_date = $this->data['debit_note_details']['debit_note_date'];
            }

            if(!empty($this->data['debit_note_details']['payment_due'])){
                $this->debit_note_details->payment_due = $this->data['debit_note_details']['payment_due'];
            }
            $this->debit_note_details->billing_email = $this->data['debit_note_details']['billing_email'];
            $this->debit_note_details->debit_note_id = $this->debit_notes->id;
            $this->debit_note_details->save();
        }
    }

    public function updateSummary(){
        $this->debit_note_summary = DebitNoteSummary::find($this->data['debit_note_summary']['id']);
        if(!empty($this->debit_note_summary)){
            $this->debit_note_summary->debit_note_id = $this->debit_notes->id;

            foreach($this->debit_note_summary_keys as $key){
                $this->debit_note_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['debit_note_summary'][$key]);
                if(empty($this->debit_note_summary->$key)){
                    $this->debit_note_summary->$key = 0.00;
                }
            }

            $this->debit_note_summary->discount_type = $this->data['debit_note_summary']['discount_type'];
            $this->debit_note_summary->save();
        }
    }

    public function deleteDebitNoteItems(){
        foreach($this->data['deleted_debit_note_items'] as $deleted_debit_note_items){
            $debit_note_item_deleted = DebitNoteItems::find($deleted_debit_note_items['id']);

            if(!empty($debit_note_item_deleted)){
                $debit_note_item_deleted->delete();

                $debit_note_item_deleted_tax = DebitNoteItemsTaxes::find($deleted_debit_note_items['tax']['id']);

                if(!empty($debit_note_item_deleted_tax)){
                    $debit_note_item_deleted_tax->delete();
                }
            }
        }
    }
}
