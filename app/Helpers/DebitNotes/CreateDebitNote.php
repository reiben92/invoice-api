<?php

namespace App\Helpers\DebitNotes;

use App\Models\DebitNotes;
use App\Models\DebitNoteItems;
use App\Models\DebitNoteTypes;
use App\Models\DebitNoteStatuses;
use App\Models\DebitNoteCurrency;
use App\Models\DebitNoteDetails;
use App\Models\DebitNoteItemsTaxes;
use App\Models\DebitNotesCustomer;
use App\Models\DebitNoteSummary;

use App\Helpers\Counters\CreateRunningNumber;

class CreateDebitNote
{   
    protected $data=[];
    protected $debit_notes=[];
    protected $debit_note_items=[];
    protected $debit_note_purchase_order=[];
    protected $debit_note_quotation=[];
    protected $debit_note_summary=[];
    protected $debit_note_details=[];
    protected $debit_note_customer=[];

    protected $debit_note_items_keys = ['amount','discount','quantity','rate'];
    protected $debit_note_summary_keys = ['total_tax','grand_total','total_discount','sub_total','discount_amount','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->create();
        $this->createItems();
        $this->createCustomer();
        $this->createDetails();
        $this->createSummary();

        return $this->debit_notes;
    }

    public function create(){
        
        $running_number = new CreateRunningNumber('Debit Note','DB');

        $this->debit_notes = new DebitNotes;
        $this->debit_notes->debit_note_no = $running_number->generateDebitNoteNo();

        if(empty($this->data['debit_notes']['status_id'])){
            $this->debit_notes->debit_note_status_id = 1;
        }

        $this->debit_notes->save();
    }

    public function createItems(){
        foreach($this->data['debit_note_items'] as $item){
            $debit_note_items = new DebitNoteItems;

            foreach($this->debit_note_items_keys as $key){
                $debit_note_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($debit_note_items->$key)){
                    $debit_note_items->$key = 0.00;
                }
            }

            $debit_note_items->description = $item['description'];
            if(!empty($item['debit_note_type_id'])){
                $debit_note_items->debit_note_type_id = $item['debit_note_type_id'];
            }

            if(!empty($item['product_id'])){
                $debit_note_items->product_id = $item['product_id'];
            }

            $debit_note_items->debit_note_id = $this->debit_notes->id;
            $debit_note_items->save();

            if(!empty($item['tax'])){
                if(!empty($item['tax']['tax_id'])){
                    $debit_note_item_taxes = new DebitNoteItemsTaxes;
                    $debit_note_item_taxes->tax_type_id = $item['tax']['tax_id'];
                    $debit_note_item_taxes->tax_rate = $item['tax']['default_tax_rate'];
                    $debit_note_item_taxes->debit_note_item_id = $debit_note_items->id;
                    $debit_note_item_taxes->debit_note_id = $this->debit_notes->id;

                    $debit_note_item_taxes->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
                    if(empty($debit_note_item_taxes->amount)){
                        $debit_note_item_taxes->amount = 0.00;
                    }

                    $debit_note_item_taxes->save();

                    $debit_note_items['debit_note_items_taxes'] = $debit_note_item_taxes;
                }
            }

           
            array_push($this->debit_note_items,$debit_note_items);
        }
    }

    public function createCustomer(){
        $this->debit_note_customer = new DebitNotesCustomer;
        $this->debit_note_customer->customer_id = $this->data['debit_note_customer']['id'];
        $this->debit_note_customer->debit_note_id = $this->debit_notes->id;
        $this->debit_note_customer->save();
    }

    public function createDetails(){
        $this->debit_note_details = new DebitNoteDetails;
        $this->debit_note_details->billing_address1 = $this->data['debit_note_details']['billing_address_line1'];
        $this->debit_note_details->billing_address2 = $this->data['debit_note_details']['billing_address_line2'];
        $this->debit_note_details->billing_address3 = $this->data['debit_note_details']['billing_address_line3'];
        $this->debit_note_details->billing_city = $this->data['debit_note_details']['billing_city'];
        $this->debit_note_details->billing_postcode = $this->data['debit_note_details']['billing_postcode'];
        $this->debit_note_details->billing_state = $this->data['debit_note_details']['billing_state'];
        $this->debit_note_details->billing_country = $this->data['debit_note_details']['billing_country'];
        $this->debit_note_details->debit_note_date = $this->data['debit_note_details']['debit_note_date'];
        $this->debit_note_details->payment_due = $this->data['debit_note_details']['payment_due'];
        $this->debit_note_details->billing_email = $this->data['debit_note_details']['billing_email'];
        $this->debit_note_details->debit_note_id = $this->debit_notes->id;
        $this->debit_note_details->save();
    }


    public function createSummary(){
        $this->debit_note_summary = new DebitNoteSummary;
        $this->debit_note_summary->debit_note_id = $this->debit_notes->id;

        foreach($this->debit_note_summary_keys as $key){
            $this->debit_note_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['debit_note_summary'][$key]);
            if(empty($this->debit_note_summary->$key)){
                $this->debit_note_summary->$key = 0.00;
            }
        }

        $this->debit_note_summary->discount_type = $this->data['debit_note_summary']['discount_type'];
        $this->debit_note_summary->save();
    }
}
