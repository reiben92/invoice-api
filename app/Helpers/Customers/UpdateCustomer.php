<?php

namespace App\Helpers\Customers;

use App\Models\Customers;
use App\Models\CustomerDetails;
use App\Models\CustomerAddresses;
use App\Models\CustomerContacts;
use App\Models\CustomerBalances;

class UpdateCustomer
{   
    protected $data=[];
    protected $customer_addresses=[];
    protected $customer_details=[];
    protected $customer_contacts=[];
    protected $customer_balances=[];

    protected $customer_details_keys = ['website','email','mobile_no','phone_no','fax','type','nickname','default_payment'];
    protected $customer_contacts_keys = ['first_name','last_name','title'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->update();
        $this->updateAddresses();
        $this->updateDetails();
        $this->updateContacts();
        $this->updateBalances();
    }

    public function update(){
        $this->customers = Customers::find($this->data['customer_id']);
    }

    public function updateAddresses(){
        $this->customer_addresses = CustomerAddresses::find($this->data['customer_addresses']['id']);

        if(!empty($this->customer_addresses)){
            $this->customer_addresses->billing_address1 = $this->data['customer_addresses']['billing_address1'];
            $this->customer_addresses->billing_address2 = $this->data['customer_addresses']['billing_address2'];
            $this->customer_addresses->billing_address3 = $this->data['customer_addresses']['billing_address3'];
            $this->customer_addresses->billing_city = $this->data['customer_addresses']['billing_city'];
            $this->customer_addresses->billing_postcode = $this->data['customer_addresses']['billing_postcode'];
            $this->customer_addresses->billing_state = $this->data['customer_addresses']['billing_state'];
            $this->customer_addresses->billing_country = $this->data['customer_addresses']['billing_country'];

            if($this->data['customer_addresses']['shipping_is_same_as_billing']=='true'){
                $this->customer_addresses->shipping_is_same_as_billing=1;
                $this->customer_addresses->shipping_address1 = $this->data['customer_addresses']['billing_address1'];
                $this->customer_addresses->shipping_address2 = $this->data['customer_addresses']['billing_address2'];
                $this->customer_addresses->shipping_address3 = $this->data['customer_addresses']['billing_address3'];
                $this->customer_addresses->shipping_city = $this->data['customer_addresses']['billing_city'];
                $this->customer_addresses->shipping_postcode = $this->data['customer_addresses']['billing_postcode'];
                $this->customer_addresses->shipping_state = $this->data['customer_addresses']['billing_state'];
                $this->customer_addresses->shipping_country = $this->data['customer_addresses']['billing_country'];
            }
            else{
                $this->customer_addresses->shipping_is_same_as_billing=0;
                $this->customer_addresses->shipping_address1 = $this->data['customer_addresses']['shipping_address1'];
                $this->customer_addresses->shipping_address2 = $this->data['customer_addresses']['shipping_address2'];
                $this->customer_addresses->shipping_address3 = $this->data['customer_addresses']['shipping_address3'];
                $this->customer_addresses->shipping_city = $this->data['customer_addresses']['shipping_city'];
                $this->customer_addresses->shipping_postcode = $this->data['customer_addresses']['shipping_postcode'];
                $this->customer_addresses->shipping_state = $this->data['customer_addresses']['shipping_state'];
                $this->customer_addresses->shipping_country = $this->data['customer_addresses']['shipping_country'];
            }

            $this->customer_addresses->save();
        }
    }

    public function updateDetails(){
        $this->customer_details = CustomerDetails::find($this->data['customer_details']['id']);
            if(!empty($this->customer_details)){
            foreach($this->customer_details_keys as $keys){
                if($this->data['customer_details'][$keys]){
                    $this->customer_details->$keys = $this->data['customer_details'][$keys];
                }
            }

            if($this->customer_details->type=="Company"){
                $this->customer_details->customer_name=$this->data['customer_details']['customer_name'];
                $this->customer_details->tax_registration_no=$this->data['customer_details']['tax_registration_no'];
                $this->customer_details->roc_no=$this->data['customer_details']['roc_no'];
            }
            
            $this->customer_details->save();
        }
    }

    public function updateContacts(){
        $this->customer_contacts = CustomerContacts::find($this->data['customer_contacts']['id']);

        if(!empty($this->customer_contacts)){
            foreach($this->customer_contacts_keys as $keys){
                $this->customer_contacts->$keys = $this->data['customer_contacts'][$keys];
            }
            $this->customer_contacts->save();

            if($this->customer_details->type=="Individual"){
                $this->customer_details->customer_name=$this->data['customer_contacts']['first_name'].' '.$this->data['customer_contacts']['last_name'];
                $this->customer_details->save();
            }
        }
    }

    public function updateBalances(){
        $this->customer_balances = CustomerBalances::find($this->data['customer_balances']['id']);

        if(!empty($this->customer_balances)){
            $this->customer_balances->open_balance = preg_replace("/[^0-9.]/", "",$this->data['customer_balances']['open_balance']);

            if(empty($this->customer_balances->open_balance)){
                $this->customer_balances->open_balance=0.00;
            }

            $this->customer_balances->open_balance_date = $this->data['customer_balances']['open_balance_date'];
            $this->customer_balances->save();
        }
    }
}
