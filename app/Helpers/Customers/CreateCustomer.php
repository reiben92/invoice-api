<?php

namespace App\Helpers\Customers;

use App\Models\Customers;
use App\Models\CustomerDetails;
use App\Models\CustomerAddresses;
use App\Models\CustomerContacts;
use App\Models\CustomerBalances;

class CreateCustomer
{   
    protected $data=[];
    protected $customer_addresses=[];
    protected $customer_details=[];
    protected $customer_contacts=[];
    protected $customer_balances=[];

    protected $customer_details_keys = ['website','email','mobile_no','phone_no','fax','type','nickname','default_payment','default_currency'];
    protected $customer_contacts_keys = ['first_name','last_name','title'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->create();
        $this->createAddresses();
        $this->createDetails();
        $this->createContacts();
        $this->createBalances();
    }

    public function create(){
        $this->customers = new Customers;
        $this->customers->customer_code = 'C000000000'.(Customers::max('id')+1);
        $this->customers->save();
    }

    public function createAddresses(){
        $this->customer_addresses = new CustomerAddresses;
        $this->customer_addresses->billing_address1 = $this->data['customer_addresses']['billing_address1'];
        $this->customer_addresses->billing_address2 = $this->data['customer_addresses']['billing_address2'];
        $this->customer_addresses->billing_address3 = $this->data['customer_addresses']['billing_address3'];
        $this->customer_addresses->billing_city = $this->data['customer_addresses']['billing_city'];
        $this->customer_addresses->billing_postcode = $this->data['customer_addresses']['billing_postcode'];
        $this->customer_addresses->billing_state = $this->data['customer_addresses']['billing_state'];
        $this->customer_addresses->billing_country = $this->data['customer_addresses']['billing_country'];
        $this->customer_addresses->customer_id = $this->customers->id;

        if($this->data['customer_addresses']['shipping_is_same_as_billing']=='true'){
            $this->customer_addresses->shipping_is_same_as_billing=1;
            $this->customer_addresses->shipping_address1 = $this->data['customer_addresses']['billing_address1'];
            $this->customer_addresses->shipping_address2 = $this->data['customer_addresses']['billing_address2'];
            $this->customer_addresses->shipping_address3 = $this->data['customer_addresses']['billing_address3'];
            $this->customer_addresses->shipping_city = $this->data['customer_addresses']['billing_city'];
            $this->customer_addresses->shipping_postcode = $this->data['customer_addresses']['billing_postcode'];
            $this->customer_addresses->shipping_state = $this->data['customer_addresses']['billing_state'];
            $this->customer_addresses->shipping_country = $this->data['customer_addresses']['billing_country'];
        }
        else{
            $this->customer_addresses->shipping_is_same_as_billing=0;
            $this->customer_addresses->shipping_address1 = $this->data['customer_addresses']['shipping_address1'];
            $this->customer_addresses->shipping_address2 = $this->data['customer_addresses']['shipping_address2'];
            $this->customer_addresses->shipping_address3 = $this->data['customer_addresses']['shipping_address3'];
            $this->customer_addresses->shipping_city = $this->data['customer_addresses']['shipping_city'];
            $this->customer_addresses->shipping_postcode = $this->data['customer_addresses']['shipping_postcode'];
            $this->customer_addresses->shipping_state = $this->data['customer_addresses']['shipping_state'];
            $this->customer_addresses->shipping_country = $this->data['customer_addresses']['shipping_country'];
        }

        $this->customer_addresses->save();
    }

    public function createDetails(){
        $this->customer_details = new CustomerDetails;
        foreach($this->customer_details_keys as $keys){
            $this->customer_details->$keys = $this->data['customer_details'][$keys];
        }

        if($this->customer_details->type=="Company"){
            $this->customer_details->customer_name=$this->data['customer_details']['customer_name'];
            $this->customer_details->tax_registration_no=$this->data['customer_details']['tax_registration_no'];
            $this->customer_details->roc_no=$this->data['customer_details']['roc_no'];
        }

        $this->customer_details->customer_id = $this->customers->id;
        $this->customer_details->save();
    }

    public function createContacts(){
        $this->customer_contacts = new CustomerContacts;
        foreach($this->customer_contacts_keys as $keys){
            $this->customer_contacts->$keys = $this->data['customer_contacts'][$keys];
        }
        $this->customer_contacts->customer_id = $this->customers->id;
        $this->customer_contacts->save();

        if($this->customer_details->type=="Individual"){
            $this->customer_details->customer_name=$this->data['customer_contacts']['first_name'].' '.$this->data['customer_contacts']['last_name'];
            $this->customer_details->save();
        }
    }

    public function createBalances(){
        $this->customer_balances = new CustomerBalances;
        $this->customer_balances->customer_id = $this->customers->id;
        $this->customer_balances->open_balance = preg_replace("/[^0-9.]/", "",$this->data['customer_balances']['open_balance']);

        if(empty($this->customer_balances->open_balance)){
            $this->customer_balances->open_balance=0.00;
        }

        $this->customer_balances->open_balance_date = $this->data['customer_balances']['open_balance_date'];
        $this->customer_balances->save();
    }
}
