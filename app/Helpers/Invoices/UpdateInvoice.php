<?php

namespace App\Helpers\Invoices;

use App\Models\Invoices;
use App\Models\InvoiceItems;
use App\Models\InvoiceTypes;
use App\Models\InvoiceStatuses;
use App\Models\InvoiceCurrency;
use App\Models\InvoiceDetails;
use App\Models\InvoiceItemsTaxes;
use App\Models\InvoiceStatementOfAccount;
use App\Models\InvoicesCustomer;
use App\Models\InvoicePurchaseOrder;
use App\Models\InvoiceQuotation;
use App\Models\InvoiceSummary;

class UpdateInvoice
{   
    protected $data=[];
    protected $invoices=[];
    protected $invoice_items=[];
    protected $invoice_purchase_order=[];
    protected $invoice_quotation=[];
    protected $invoice_summary=[];
    protected $invoice_details=[];
    protected $invoice_customer=[];

    protected $invoice_items_keys = ['amount','discount','quantity','rate'];
    protected $invoice_summary_keys = ['total_tax','grand_total','total_discount','sub_total','discount_amount','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->update();
        $this->updateItems();
        $this->updateCustomer();
        $this->updateDetails();
        $this->updateSummary();
        $this->updateInvoiceQuotation();
        $this->updateInvoicePurchaseOrder();
        $this->deleteInvoiceItems();
    }

    public function update(){
        $this->invoices = Invoices::find($this->data['invoice']['id']);
        //$this->invoices->invoice_no = $this->data['invoice']['invoice_no'];

        if(empty($this->data['invoice']['status_id'])){
            $this->invoices->invoice_status_id = 1;
        }

        $this->invoices->save();
    }

    public function updateItems(){
        foreach($this->data['invoice_items'] as $item){
            if($item['id']=='NEW'){
                $invoice_items = new InvoiceItems;
            }
            else{
                $invoice_items = InvoiceItems::find($item['id']);
            }

            foreach($this->invoice_items_keys as $key){
                $invoice_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($invoice_items->$key)){
                    $invoice_items->$key = 0.00;
                }
            }

            $invoice_items->description = $item['description'];
            if(!empty($item['invoice_type_id'])){
                $invoice_items->invoice_type_id = $item['invoice_type_id'];
            }

            if(!empty($item['product_id'])){
                $invoice_items->product_id = $item['product_id'];
            }

            $invoice_items->invoice_id = $this->invoices->id;
            $invoice_items->save();

            $invoice_item_taxes = [];

            if(!empty($item['tax'])){
                if(!empty($item['tax']['id'])){
                    $invoice_item_taxes = InvoiceItemsTaxes::find($item['tax']['id']);

                    if(empty($item['tax']['tax_id'])){
                        if(!empty($invoice_item_taxes)){
                            $invoice_item_taxes->delete();
                        }
                    }
                    else{
                        if(!empty($invoice_item_taxes)){
                            $invoice_item_taxes = $this->updateInvoiceTaxItem($invoice_item_taxes,$item,$invoice_items);
                        }
                    }
                }
                else{
                    $invoice_item_taxes = new InvoiceItemsTaxes;
                    $invoice_item_taxes = $this->updateInvoiceTaxItem($invoice_item_taxes,$item,$invoice_items);
                }

                $invoice_items['invoice_items_taxes'] = $invoice_item_taxes;
            }
           
            array_push($this->invoice_items,$invoice_items);
        }
    }

    public function updateInvoiceTaxItem($objs,$item,$invoice_items){
        $objs->tax_type_id = $item['tax']['tax_id'];
        $objs->tax_rate = $item['tax']['default_tax_rate'];
        $objs->invoice_item_id = $invoice_items->id;
        $objs->invoice_id = $this->invoices->id;

        $objs->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
        if(empty($objs->amount)){
            $objs->amount = 0.00;
        }

        $objs->save();
        return $objs;
    }

    public function updateCustomer(){
        $this->invoice_customer = InvoicesCustomer::find($this->data['invoice_customer']['id']);
        if(!empty($this->invoice_customer)){
            $this->invoice_customer->customer_id = $this->data['invoice_customer']['customer_id'];
            $this->invoice_customer->invoice_id = $this->invoices->id;
            $this->invoice_customer->save();
        }
    }

    public function updateDetails(){
        $this->invoice_details = InvoiceDetails::find($this->data['invoice_details']['id']);
        if(!empty($this->invoice_details)){
            $this->invoice_details->billing_address1 = $this->data['invoice_details']['billing_address_line1'];
            $this->invoice_details->billing_address2 = $this->data['invoice_details']['billing_address_line2'];
            $this->invoice_details->billing_address3 = $this->data['invoice_details']['billing_address_line3'];
            $this->invoice_details->billing_city = $this->data['invoice_details']['billing_city'];
            $this->invoice_details->billing_postcode = $this->data['invoice_details']['billing_postcode'];
            $this->invoice_details->billing_state = $this->data['invoice_details']['billing_state'];
            $this->invoice_details->billing_country = $this->data['invoice_details']['billing_country'];
            if(!empty($this->data['invoice_details']['invoice_date'])){
                $this->invoice_details->invoice_date = $this->data['invoice_details']['invoice_date'];
            }

            if(!empty($this->data['invoice_details']['payment_due'])){
                $this->invoice_details->payment_due = $this->data['invoice_details']['payment_due'];
            }
            $this->invoice_details->billing_email = $this->data['invoice_details']['billing_email'];
            $this->invoice_details->invoice_id = $this->invoices->id;
            $this->invoice_details->save();
        }
    }

    public function updateInvoicePurchaseOrder(){
        $this->invoice_purchase_order = InvoicePurchaseOrder::find($this->data['invoice_purchase_order']['id']);

        if(empty($this->data['invoice_purchase_order']['purchase_order_number'])){
            if(!empty($this->invoice_purchase_order)){
                $this->invoice_purchase_order->delete();
            }
            else{
            }
        }
        else{
            if(empty($this->invoice_purchase_order)){
                $this->invoice_purchase_order = new InvoicePurchaseOrder;
            }

            $this->invoice_purchase_order->invoice_id = $this->invoices->id;
            $this->invoice_purchase_order->purchase_order_no = $this->data['invoice_purchase_order']['purchase_order_number'];
            $this->invoice_purchase_order->save();
        }
    }

    public function updateInvoiceQuotation(){
        $this->invoice_quotation = InvoiceQuotation::find($this->data['invoice_quotation']['id']);

        if(empty($this->data['invoice_quotation']['quotation_number'])){
            if(!empty($this->invoice_quotation)){
                $this->invoice_quotation->delete();
            }
        }
        else{
            if(empty($this->invoice_quotation)){
                $this->invoice_quotation = new InvoiceQuotation;
            }
            
            $this->invoice_quotation->invoice_id = $this->invoices->id;
            $this->invoice_quotation->quotation_no = $this->data['invoice_quotation']['quotation_number'];
            $this->invoice_quotation->save();
        }
    }

    public function updateSummary(){
        $this->invoice_summary = InvoiceSummary::find($this->data['invoice_summary']['id']);
        if(!empty($this->invoice_summary)){
            $this->invoice_summary->invoice_id = $this->invoices->id;

            foreach($this->invoice_summary_keys as $key){
                $this->invoice_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['invoice_summary'][$key]);
                if(empty($this->invoice_summary->$key)){
                    $this->invoice_summary->$key = 0.00;
                }
            }

            $this->invoice_summary->currency_id = $this->data['invoice_summary']['currency_id'];
            $this->invoice_summary->discount_type = $this->data['invoice_summary']['discount_type'];
            $this->invoice_summary->save();
        }
    }

    public function deleteInvoiceItems(){
        foreach($this->data['deleted_invoice_items'] as $deleted_invoice_items){
            $invoice_item_deleted = InvoiceItems::find($deleted_invoice_items['id']);

            if(!empty($invoice_item_deleted)){
                $invoice_item_deleted->delete();

                $invoice_item_deleted_tax = InvoiceItemsTaxes::find($deleted_invoice_items['tax']['id']);

                if(!empty($invoice_item_deleted_tax)){
                    $invoice_item_deleted_tax->delete();
                }
            }
        }
    }
}
