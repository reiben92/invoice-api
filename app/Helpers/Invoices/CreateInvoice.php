<?php

namespace App\Helpers\Invoices;

use App\Models\Invoices;
use App\Models\InvoiceItems;
use App\Models\InvoiceTypes;
use App\Models\InvoiceStatuses;
use App\Models\InvoiceCurrency;
use App\Models\InvoiceDetails;
use App\Models\InvoiceItemsTaxes;
use App\Models\InvoiceStatementOfAccount;
use App\Models\InvoicesCustomer;
use App\Models\InvoicePurchaseOrder;
use App\Models\InvoiceQuotation;
use App\Models\InvoiceSummary;

use App\Helpers\Counters\CreateRunningNumber;

class CreateInvoice
{   
    protected $data=[];
    protected $invoices=[];
    protected $invoice_items=[];
    protected $invoice_purchase_order=[];
    protected $invoice_quotation=[];
    protected $invoice_summary=[];
    protected $invoice_details=[];
    protected $invoice_customer=[];

    protected $invoice_items_keys = ['amount','discount','quantity','rate'];
    protected $invoice_summary_keys = ['total_tax','grand_total','total_discount','sub_total','discount_amount','balance_due'];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->create();
        $this->createItems();
        $this->createCustomer();
        $this->createDetails();
        $this->createInvoiceQuotation();
        $this->createInvoicePurchaseOrder();
        $this->createSummary();
        $this->createInvoiceStatement();

        return $this->invoices;
    }

    public function create(){
        
        $running_number = new CreateRunningNumber('Invoice','I');

        $this->invoices = new Invoices;
        $this->invoices->invoice_no = $running_number->generateInvoiceNo();

        if(empty($this->data['invoice']['status_id'])){
            $this->invoices->invoice_status_id = 2;
        }

        $this->invoices->save();
    }

    public function createItems(){
        foreach($this->data['invoice_items'] as $item){
            $invoice_items = new InvoiceItems;

            foreach($this->invoice_items_keys as $key){
                $invoice_items->$key = preg_replace("/[^0-9.]/", "",$item[$key]);
                if(empty($invoice_items->$key)){
                    $invoice_items->$key = 0.00;
                }
            }

            $invoice_items->description = $item['description'];
            if(!empty($item['invoice_type_id'])){
                $invoice_items->invoice_type_id = $item['invoice_type_id'];
            }

            if(!empty($item['product_id'])){
                $invoice_items->product_id = $item['product_id'];
            }

            $invoice_items->invoice_id = $this->invoices->id;
            $invoice_items->save();

            if(!empty($item['tax'])){
                if(!empty($item['tax']['tax_id'])){
                    $invoice_item_taxes = new InvoiceItemsTaxes;
                    $invoice_item_taxes->tax_type_id = $item['tax']['tax_id'];
                    $invoice_item_taxes->tax_rate = $item['tax']['default_tax_rate'];
                    $invoice_item_taxes->invoice_item_id = $invoice_items->id;
                    $invoice_item_taxes->invoice_id = $this->invoices->id;

                    $invoice_item_taxes->amount = preg_replace("/[^0-9.]/", "",$item['tax']['amount']);
                    if(empty($invoice_item_taxes->amount)){
                        $invoice_item_taxes->amount = 0.00;
                    }

                    $invoice_item_taxes->save();

                    $invoice_items['invoice_items_taxes'] = $invoice_item_taxes;
                }
            }

           
            array_push($this->invoice_items,$invoice_items);
        }
    }

    public function createCustomer(){
        $this->invoice_customer = new InvoicesCustomer;
        $this->invoice_customer->customer_id = $this->data['invoice_customer']['customer_id'];
        $this->invoice_customer->invoice_id = $this->invoices->id;
        $this->invoice_customer->save();
    }

    public function createDetails(){
        $this->invoice_details = new InvoiceDetails;
        $this->invoice_details->billing_address1 = $this->data['invoice_details']['billing_address_line1'];
        $this->invoice_details->billing_address2 = $this->data['invoice_details']['billing_address_line2'];
        $this->invoice_details->billing_address3 = $this->data['invoice_details']['billing_address_line3'];
        $this->invoice_details->billing_city = $this->data['invoice_details']['billing_city'];
        $this->invoice_details->billing_postcode = $this->data['invoice_details']['billing_postcode'];
        $this->invoice_details->billing_state = $this->data['invoice_details']['billing_state'];
        $this->invoice_details->billing_country = $this->data['invoice_details']['billing_country'];
        $this->invoice_details->invoice_date = $this->data['invoice_details']['invoice_date'];
        $this->invoice_details->payment_due = $this->data['invoice_details']['payment_due'];
        $this->invoice_details->billing_email = $this->data['invoice_details']['billing_email'];
        $this->invoice_details->invoice_id = $this->invoices->id;
        $this->invoice_details->save();
    }

    public function createInvoicePurchaseOrder(){
        if(!empty($this->data['invoice_purchase_order'])){
            if(!empty($this->data['invoice_purchase_order']['purchase_order_number'])){
                $this->invoice_purchase_order = new InvoicePurchaseOrder;
                $this->invoice_purchase_order->invoice_id = $this->invoices->id;
                $this->invoice_purchase_order->purchase_order_no = $this->data['invoice_purchase_order']['purchase_order_number'];
                $this->invoice_purchase_order->save();
            }
        }
    }

    public function createInvoiceQuotation(){
        if(!empty($this->data['invoice_quotation'])){
            if(!empty($this->data['invoice_quotation']['quotation_number'])){
                $this->invoice_quotation = new InvoiceQuotation;
                $this->invoice_quotation->invoice_id = $this->invoices->id;
                $this->invoice_quotation->quotation_no = $this->data['invoice_quotation']['quotation_number'];
                $this->invoice_quotation->save();
            }
        }
    }

    public function createSummary(){
        $this->invoice_summary = new InvoiceSummary;
        $this->invoice_summary->invoice_id = $this->invoices->id;

        foreach($this->invoice_summary_keys as $key){
            $this->invoice_summary->$key = preg_replace("/[^0-9.]/", "",$this->data['invoice_summary'][$key]);
            if(empty($this->invoice_summary->$key)){
                $this->invoice_summary->$key = 0.00;
            }
        }

        $this->invoice_summary->currency_id = $this->data['invoice_summary']['currency_id'];
        $this->invoice_summary->discount_type = $this->data['invoice_summary']['discount_type'];
        $this->invoice_summary->save();
    }

    public function createInvoiceStatement(){
        $this->invoice_statement = new InvoiceStatementOfAccount;
        $this->invoice_statement->invoice_id = $this->invoices->id;
        $this->invoice_statement->new_charges = $this->invoice_summary->balance_due;
        $this->invoice_statement->save();
    }
}
