<?php

namespace App\Helpers\Recurring;

use App\Models\InvoiceRecurring;
use App\Models\InvoiceRecurringPivot;
use App\Helpers\Invoices\CreateInvoiceRecurring;

use DateTime;

class CreateRecurring
{   
    protected $data=[];
    protected $invoice_recurring=[];

    public function __construct(array $data){
        $this->data = $data;
    }

    public function init(){
        $this->create();

        //return $this->wew;
        return $this->invoice_recurring;
    }

    public function create(){
        $this->invoice_recurring = new InvoiceRecurring;
        $this->invoice_recurring->interval = $this->data['invoice_recurring']['interval'];
        $this->invoice_recurring->interval_value = $this->data['invoice_recurring']['interval_value'];
        $this->invoice_recurring->payment_terms = $this->data['invoice_recurring']['payment_terms'];
        $this->invoice_recurring->start_date = date("Y-m-d",strtotime(str_replace('/','-',$this->data['invoice_recurring']['start_date'])));
        $this->invoice_recurring->end_date = date("Y-m-d",strtotime(str_replace('/','-',$this->data['invoice_recurring']['end_date'])));
        $this->invoice_recurring->save();
        $this->invoice_recurring->gap = $this->gapBetweenDates();

        if($this->invoice_recurring->interval=='Monthly'){
            $this->createMonthly();
        }
        else if($this->invoice_recurring->interval=='Weekly'){
            $this->createWeekly();
        }
        else if($this->invoice_recurring->interval=='Daily'){
            $this->createDaily();
        }
    }

    public function createDaily(){
        for($i=1;$i<=$this->invoice_recurring->gap->days;$i++){
            $d1 = DateTime::createFromFormat('Y-m-d',$this->invoice_recurring->start_date);
            $d1->modify('+'.$i.' day');

            $CreateInvoice = new CreateInvoiceRecurring($this->data,$d1->format('d/m/Y'),$this->invoice_recurring);
            $CreateInvoice->init();
        }
    }

    public function createWeekly(){
        for($i=1;$i<=$this->invoice_recurring->gap->days;$i++){
            $d1 = DateTime::createFromFormat('Y-m-d',$this->invoice_recurring->start_date);
            $d1->modify('+'.$i.' day');
            $current_l = $d1->format('l');

            if($current_l==$this->invoice_recurring->interval_value){
                $CreateInvoice = new CreateInvoiceRecurring($this->data,$d1->format('d/m/Y'),$this->invoice_recurring);
                $CreateInvoice->init();
            }
        }
    }

    public function createMonthly(){
        for($i=1;$i<=$this->invoice_recurring->gap->days;$i++){
            $d1 = DateTime::createFromFormat('Y-m-d',$this->invoice_recurring->start_date);
            $d1->modify('+'.$i.' day');
            $current_d = $d1->format('j');

            if($current_d==$this->invoice_recurring->interval_value){
                $CreateInvoice = new CreateInvoiceRecurring($this->data,$d1->format('d/m/Y'),$this->invoice_recurring);
                $CreateInvoice->init();
            }
        }
    }

    public function gapBetweenDates(){
        $date1 = DateTime::createFromFormat('Y-m-d',$this->invoice_recurring->start_date);
        $date2 = DateTime::createFromFormat('Y-m-d',$this->invoice_recurring->end_date);

        return $date1->diff($date2);
    }

}
