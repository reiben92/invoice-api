<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteSummary extends Models{
	use SoftDeletes;
	
    public $table = 'credit_note_summary';

    public function CreditNotes(){
    	return $this->hasOne('App\Models\CreditNotes','id','credit_note_id');
    }
}
