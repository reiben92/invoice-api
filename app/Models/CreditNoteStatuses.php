<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteStatuses extends Models{
	use SoftDeletes;
	
    public $table = 'credit_note_statuses';
}
