<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteTypes extends Models{
	use SoftDeletes;
	
    public $table = 'debit_note_types';
}
