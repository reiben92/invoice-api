<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class CreditNoteCurrency extends Models{
	use SoftDeletes;
	
    public $table = 'credit_note_currency';

    public function CreditNotes(){
    	return $this->hasOne('App\Models\CreditNotes','id','credit_note_id');
    }

    public function Currencies(){
    	return $this->hasOne('App\Models\Currencies','id','currency_id');
    }
}
