<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccounts extends Models{
	use SoftDeletes;
	
    public $table = 'bank_accounts';
}
