<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountingDetails extends Models{
    public $table = 'accounting_details';

    use SoftDeletes;
}
