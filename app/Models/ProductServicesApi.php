<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductServicesApi extends Models{
	use SoftDeletes;

    public $table = 'product_services_api';

    public function ProductServicesApiDetails(){
    	return $this->hasOne('\App\Models\ProductServicesApiDetails','product_services_api_id','id');
    }
}
