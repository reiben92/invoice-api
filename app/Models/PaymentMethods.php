<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethods extends Models{
	use SoftDeletes;
	
    public $table = 'payment_methods';
}
