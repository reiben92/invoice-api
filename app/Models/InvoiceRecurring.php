<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class InvoiceRecurring extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_recurring';

    public function Invoices(){
    	return $this->belongsToMany('App\Models\Invoices', 'invoice_recurring_pivot', 'invoice_id', 'id');
    }

    public function InvoiceRecurringPivot(){
    	return $this->hasMany('App\Models\InvoiceRecurringPivot','invoice_recurring_id','id');
    }
}
