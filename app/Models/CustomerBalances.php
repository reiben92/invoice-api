<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class CustomerBalances extends Models{
	use SoftDeletes;
    public $table = 'customer_balances';

    public function Customer(){
    	return $this->hasOne('App\Models\CustomerBalances','id','customer_id');
    }
}
