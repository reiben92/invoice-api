<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptTypes extends Models{
	use SoftDeletes;
	
    public $table = 'receipt_types';
}
