<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptStatuses extends Models{
	use SoftDeletes;
	
    public $table = 'receipt_statuses';
}
