<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductServices extends Models{
    public $table = 'product_services';

    use SoftDeletes;

    public function ProductServicesDetails(){
    	return $this->hasMany('\App\Models\ProductServicesDetails','product_id','id');
    }

    public function ProductServicesStatus(){
        return $this->hasMany('\App\Models\ProductServicesStatus','id','status_id');
    }

    public function ProductServicesApiDetails(){
    	return $this->hasOne('\App\Models\ProductServicesApiDetails','product_services_id','id');
    }
}
