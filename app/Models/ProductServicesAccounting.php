<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class ProductServicesAccounting extends Models{
	use SoftDeletes;
	
    public $table = 'product_services_accounting';

    public function ProductServicesDetails(){
    	return $this->hasOne('\App\Models\ProductServicesDetails','id','product_detail_id');
    }

    public function AccountingDetails(){
    	return $this->hasOne('\App\Models\AccountingDetails','id','accounting_id');
    }
}
