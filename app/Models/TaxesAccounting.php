<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxesAccounting extends Models{
	use SoftDeletes;
	
    public $table = 'taxes';

    public function Taxes(){
    	return $this->hasOne('App\Models\Taxes','id','tax_id');
    }

    public function AccountingDetails(){
    	return $this->hasOne('App\Models\AccountingDetails','id','accounting_id');
    }
}
