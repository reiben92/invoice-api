<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductServicesApiDetails extends Models{
	use SoftDeletes;

    public $table = 'product_services_api_details';

    public function ProductServices(){
    	return $this->hasOne('\App\Models\ProductServices','id','product_services_id');
    }

    public function ProductServicesApi(){
    	return $this->hasOne('\App\Models\ProductServicesApi','id','product_services_api_id');
    }
}
