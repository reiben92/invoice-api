<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class InvoicePurchaseOrder extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_purchase_order';

    public function Invoice(){
    	return $this->hasOne('App\Models\Invoices','id','invoice_id');
    }
}
