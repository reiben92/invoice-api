<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceQuotation extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_quotation';

    public function Invoice(){
    	return $this->hasOne('App\Models\Invoices','id','invoice_id');
    }
}
