<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class InvoiceDetails extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_details';

    public function Invoice(){
    	return $this->hasOne('App\Models\Invoice','id','invoice_id');
    }
}
