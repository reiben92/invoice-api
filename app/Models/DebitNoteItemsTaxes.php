<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteItemTaxes extends Models{
	use SoftDeletes;
	
    public $table = 'debit_note_items_taxes';

    public function DebitNoteItems(){
    	return $this->hasMany('App\Models\DebitNoteItems','id','debit_note_item_id');
    }

    public function Taxes(){
    	return $this->hasOne('App\Models\Taxes','id','tax_type_id');
    }
}
