<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductServicesTypes extends Models{
	use SoftDeletes;

    public $table = 'product_services_types';

    public function ProductServicesDetails(){
    	return $this->hasOne('\App\Models\ProductServicesDetails','product_type_id','id');
    }
}
