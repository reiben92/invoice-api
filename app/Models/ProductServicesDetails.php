<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductServicesDetails extends Models{
    public $table = 'product_services_details';

    use SoftDeletes;

    public function ProductServicesAccounting(){
    	return $this->hasMany('\App\Models\ProductServicesAccounting','product_detail_id','id');
    }

    public function ProductServicesType(){
    	return $this->hasOne('\App\Models\ProductServicesTypes','id','product_type_id');
    }
}
