<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceStatementOfAccount extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_statement_of_account';
}
