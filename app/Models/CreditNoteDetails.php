<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class CreditNoteDetails extends Models{
	use SoftDeletes;
	
    public $table = 'credit_note_details';

    public function CreditNotes(){
    	return $this->hasOne('App\Models\CreditNotes','id','credit_note_id');
    }
}
