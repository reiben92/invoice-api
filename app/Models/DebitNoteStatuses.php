<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteStatuses extends Models{
	use SoftDeletes;
	
    public $table = 'debit_note_statuses';
}
