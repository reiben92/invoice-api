<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class CustomerContacts extends Models{
	use SoftDeletes;
	
    public $table = 'customer_contacts';

    public function Customer(){
    	return $this->hasOne('App\Models\Customers','id','customer_id');
    }
}
