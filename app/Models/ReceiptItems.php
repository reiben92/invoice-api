<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptItems extends Models{
    use SoftDeletes;
    
    public $table = 'receipt_items';

    public function Receipt(){
    	return $this->hasOne('App\Models\Receipts','id','receipt_id');
    }

    public function ReceiptType(){
    	return $this->hasOne('App\Models\ReceiptTypes','id','receipt_type_id');
    }

    public function ReceiptTaxes(){
    	return $this->hasMany('App\Models\ReceiptItemsTaxes','receipt_item_id','id');
    }

    public function Invoices(){
        return $this->hasOne('App\Models\Invoices','id','invoice_id');
    }
}
