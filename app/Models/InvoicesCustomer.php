<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoicesCustomer extends Models{
	use SoftDeletes;
	
    public $table = 'invoices_customer';

    public function Invoices(){
    	return $this->hasOne('App\Models\Invoices','id','invoice_id');
    }

    public function Customer(){
    	return $this->hasOne('App\Models\Customers','id','customer_id');
    }
}
