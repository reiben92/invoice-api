<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class Customers extends Models{
    use SoftDeletes;
    
    public $table = 'customers';

    public function CustomerDetails(){
    	return $this->hasMany('App\Models\CustomerDetails','customer_id','id');
    }

    public function CustomerAddresses(){
    	return $this->hasMany('App\Models\CustomerAddresses','customer_id','id');
    }

    public function CustomerContacts(){
    	return $this->hasMany('App\Models\CustomerContacts','customer_id','id');
    }

    public function CustomerBalances(){
        return $this->hasOne('App\Models\CustomerBalances','customer_id','id');
    }
}
