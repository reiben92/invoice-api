<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class CustomerDetails extends Models{
	use SoftDeletes;
	
    public $table = 'customer_details';

    public function Customer(){
    	return $this->hasOne('App\Models\Customers','id','customer_id');
    }
}
