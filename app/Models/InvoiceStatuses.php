<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceStatuses extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_statuses';
}
