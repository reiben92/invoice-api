<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteCustomers extends Models{
	use SoftDeletes;
	
    public $table = 'debit_note_customers';

    public function DebitNotes(){
    	return $this->hasOne('App\Models\DebitNotes','id','debit_note_id');
    }

    public function Customer(){
    	return $this->hasOne('App\Models\Customers','id','customer_id');
    }
}
