<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceItemsTaxes extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_items_taxes';

    public function InvoiceItems(){
    	return $this->hasMany('App\Models\InvoiceItems','id','invoice_item_id');
    }

    public function Taxes(){
    	return $this->hasOne('App\Models\Taxes','id','tax_type_id');
    }
}
