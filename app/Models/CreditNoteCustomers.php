<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteCustomers extends Models{
	use SoftDeletes;
	
    public $table = 'credit_note_customer';

    public function CreditNotes(){
    	return $this->hasOne('App\Models\CreditNotes','id','credit_note_id');
    }

    public function Customer(){
    	return $this->hasOne('App\Models\Customers','id','customer_id');
    }
}
