<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteTypes extends Models{
	use SoftDeletes;
	
    public $table = 'credit_note_types';
}
