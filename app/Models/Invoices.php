<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoices extends Models{
    use SoftDeletes;
    
    public $table = 'invoices';

    public function InvoiceItems(){
    	return $this->hasMany('\App\Models\InvoiceItems','invoice_id','id');
    }

    public function InvoiceDetails(){
    	return $this->hasOne('App\Models\InvoiceDetails','invoice_id','id');
    }

    public function InvoiceCustomers(){
    	return $this->hasMany('\App\Models\InvoicesCustomer','invoice_id','id');
    }

    public function Status(){
    	return $this->hasOne('App\Models\InvoiceStatuses','id','invoice_status_id');
    }

    public function InvoiceStatementOfAccount(){
        return $this->hasOne('\App\Models\InvoiceStatementOfAccount','invoice_id','id');
    }

    public function InvoiceSummary(){
        return $this->hasOne('\App\Models\InvoiceSummary','invoice_id','id');
    }

    public function InvoicePurchaseOrder(){
        return $this->hasOne('\App\Models\InvoicePurchaseOrder','invoice_id','id');
    }

    public function InvoiceQuotation(){
        return $this->hasOne('\App\Models\InvoiceQuotation','invoice_id','id');
    }

    public function InvoiceRecurring(){
        return $this->belongsToMany('App\Models\InvoiceRecurring', 'invoice_recurring_pivot', 'id', 'invoice_recurring_id');
    }

    public function InvoiceRecurringPivot(){
        return $this->hasMany('App\Models\InvoiceRecurringPivot','invoice_id','id');
    }
}
