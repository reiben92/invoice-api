<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptItemsTaxes extends Models{
	use SoftDeletes;
	
    public $table = 'receipt_items_taxes';

    public function ReceiptItems(){
    	return $this->hasMany('App\Models\ReceiptItems','id','receipt_item_id');
    }

    public function Taxes(){
    	return $this->hasOne('App\Models\Taxes','id','tax_type_id');
    }
}
