<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class ReceiptSummary extends Models{
	use SoftDeletes;
	
    public $table = 'receipt_summary';

    public function Receipt(){
    	return $this->hasOne('App\Models\Receipts','id','receipt_id');
    }
    
    public function Currencies(){
    	return $this->hasOne('App\Models\Currencies','id','currency_id');
    }
}
