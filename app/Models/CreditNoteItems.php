<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteItems extends Models{
    use SoftDeletes;
    
    public $table = 'credit_note_items';

    public function CreditNotes(){
    	return $this->hasOne('App\Models\CreditNotes','id','credit_note_id');
    }

    public function CreditNoteTypes(){
    	return $this->hasOne('App\Models\CreditNoteTypes','id','credit_note_type');
    }

    public function Invoices(){
        return $this->hasOne('App\Models\Invoices','id','credit_note_type_id');
    }

    public function Receipts(){
        return $this->hasOne('App\Models\Receipts','id','credit_note_type_id');
    }

    public function CreditNoteTaxes(){
    	return $this->hasMany('App\Models\CreditNoteItemsTaxes','credit_note_item_id','id');
    }
}
