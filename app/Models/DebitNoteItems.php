<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteItems extends Models{
    use SoftDeletes;
    
    public $table = 'debit_note_items';

    public function DebitNotes(){
    	return $this->hasOne('App\Models\DebitNotes','id','debit_note_id');
    }

    public function DebitNoteTypes(){
    	return $this->hasOne('App\Models\DebitNoteTypes','id','debit_note_type');
    }

    public function Invoices(){
        return $this->hasOne('App\Models\Invoices','id','debit_note_type_id');
    }

    public function Receipts(){
        return $this->hasOne('App\Models\Receipts','id','debit_note_type_id');
    }

    public function DebitNoteTaxes(){
    	return $this->hasMany('App\Models\DebitNoteTaxes','debit_note_item_id','id');
    }
}
