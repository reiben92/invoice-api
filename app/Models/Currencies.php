<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currencies extends Models{
    public $table = 'currencies';

    use SoftDeletes;
}
