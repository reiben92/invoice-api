<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNotes extends Models{
    use SoftDeletes;
    
    public $table = 'credit_notes';

    public function CreditNoteItems(){
    	return $this->hasMany('\App\Models\CreditNoteItems','credit_note_id','id');
    }

    public function CreditNoteDetails(){
    	return $this->hasOne('App\Models\CreditNoteDetails','credit_note_id','id');
    }

    public function CreditNoteCustomers(){
    	return $this->hasMany('\App\Models\CreditNoteCustomers','credit_note_id','id');
    }

    public function Status(){
    	return $this->hasOne('App\Models\CreditNoteStatuses','id','credit_note_status_id');
    }

    public function CreditNoteSummary(){
        return $this->hasOne('\App\Models\CreditNoteSummary','credit_note_id','id');
    }

}
