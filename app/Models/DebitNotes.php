<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNotes extends Models{
    use SoftDeletes;
    
    public $table = 'debit_notes';

    public function DebitNoteItems(){
    	return $this->hasMany('\App\Models\DebitNoteItems','debit_note_id','id');
    }

    public function DebitNoteDetails(){
    	return $this->hasOne('App\Models\DebitNoteDetails','debit_note_id','id');
    }

    public function DebitNoteCustomers(){
    	return $this->hasMany('\App\Models\DebitNotesCustomer','debit_note_id','id');
    }

    public function Status(){
    	return $this->hasOne('App\Models\DebitNoteStatuses','id','debit_note_status_id');
    }

    public function DebitNoteSummary(){
        return $this->hasOne('\App\Models\DebitNoteSummary','debit_note_id','id');
    }

}
