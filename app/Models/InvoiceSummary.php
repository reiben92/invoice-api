<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class InvoiceSummary extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_summary';

    public function Invoices(){
    	return $this->hasOne('App\Models\Invoices','id','invoice_id');
    }

    public function Currencies(){
    	return $this->hasOne('App\Models\Currencies','id','currency_id');
    }
}
