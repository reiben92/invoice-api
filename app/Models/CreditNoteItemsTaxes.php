<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreditNoteItemsTaxes extends Models{
	use SoftDeletes;
	
    public $table = 'credit_note_items_taxes';

    public function CreditNoteItems(){
    	return $this->hasMany('App\Models\CreditNoteItems','id','credit_note_item_id');
    }

    public function Taxes(){
    	return $this->hasOne('App\Models\Taxes','id','tax_type_id');
    }
}
