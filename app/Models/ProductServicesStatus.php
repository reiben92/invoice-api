<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductServicesStatus extends Models{
    public $table = 'product_services_status';

    use SoftDeletes;

    public function ProductServices(){
    	return $this->hasMany('\App\Models\ProductServices','status_id','id');
    }
}
