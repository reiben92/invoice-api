<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceiptCustomers extends Models{
	use SoftDeletes;
	
    public $table = 'receipt_customers';

    public function Receipts(){
    	return $this->hasOne('App\Models\Receipts','id','receipt_id');
    }

    public function Customer(){
    	return $this->hasOne('App\Models\Customers','id','customer_id');
    }
}
