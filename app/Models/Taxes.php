<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Taxes extends Models{
	use SoftDeletes;
	
    public $table = 'taxes';

   	public function TaxesAccouting(){
   		return $this->hasMany('App\Models\TaxesAccouting','tax_id','id');
   	}
}
