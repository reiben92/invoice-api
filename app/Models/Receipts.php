<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipts extends Models{
    use SoftDeletes;
    
    public $table = 'receipts';

    public function ReceiptItems(){
    	return $this->hasMany('\App\Models\ReceiptItems','receipt_id','id');
    }

    public function ReceiptDetails(){
    	return $this->hasOne('App\Models\ReceiptDetails','receipt_id','id');
    }

    public function ReceiptCustomers(){
    	return $this->hasMany('\App\Models\ReceiptCustomers','receipt_id','id');
    }

    public function Status(){
    	return $this->hasOne('App\Models\ReceiptStatuses','id','receipt_status_id');
    }

    public function ReceiptSummary(){
        return $this->hasOne('\App\Models\ReceiptSummary','receipt_id','id');
    }

    public function Receipt()
    {
        return $this->morphTo();
    }

}
