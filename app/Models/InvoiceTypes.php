<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceTypes extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_types';
}
