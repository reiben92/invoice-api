<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class InvoiceRecurringPivot extends Models{
	use SoftDeletes;
	
    public $table = 'invoice_recurring';

    public function Invoices(){
    	return $this->hasOne('App\Models\Invoices','id','invoice_id',);
    }

    public function InvoiceRecurring(){
    	return $this->hasOne('App\Models\InvoiceRecurring','id','invoice_recurring_id',);
    }
}
