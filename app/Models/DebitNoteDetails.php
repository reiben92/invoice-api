<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class DebitNoteDetails extends Models{
	use SoftDeletes;
	
    public $table = 'debit_note_details';

    public function DebitNotes(){
    	return $this->hasOne('App\Models\DebitNotes','id','debit_note_id');
    }
}
