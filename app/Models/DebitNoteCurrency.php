<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class DebitNoteCurrency extends Models{
	use SoftDeletes;
	
    public $table = 'debit_note_currency';

    public function Invoices(){
    	return $this->hasOne('App\Models\Invoices','id','invoice_id');
    }

    public function Currencies(){
    	return $this->hasOne('App\Models\Currencies','id','currency_id');
    }
}
