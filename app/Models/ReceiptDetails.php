<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Eloquent as Models;

class ReceiptDetails extends Models{
	use SoftDeletes;
	
    public $table = 'receipt_details';

    public function Receipt(){
    	return $this->hasOne('App\Models\Receipts','id','receipt_id');
    }

    public function PaymentMethods(){
    	return $this->hasOne('App\Models\PaymentMethods','id','payment_method_id');
    }

    public function BankAccounts(){
    	return $this->hasOne('App\Models\BankAccounts','id','bank_account_id');
    }
}
