<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class DebitNoteSummary extends Models{
	use SoftDeletes;
	
    public $table = 'debit_note_summary';

    public function DebitNotes(){
    	return $this->hasOne('App\Models\DebitNotes','id','debit_note_id');
    }
}
