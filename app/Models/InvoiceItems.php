<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceItems extends Models{
    use SoftDeletes;
    
    public $table = 'invoice_items';

    public function Invoice(){
    	return $this->hasOne('App\Models\Invoices','id','invoice_id');
    }

    public function InvoiceType(){
    	return $this->hasOne('App\Models\InvoiceTypes','id','invoice_type_id');
    }

    public function InvoiceTaxes(){
    	return $this->hasMany('App\Models\InvoiceItemsTaxes','invoice_item_id','id');
    }
}
