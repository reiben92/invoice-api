<?php

namespace App\Models;

use Eloquent as Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Counters extends Models{
    public $table = 'counters';

    use SoftDeletes;
}
