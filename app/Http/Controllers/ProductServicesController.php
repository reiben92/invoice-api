<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\ProductServices;
use App\Helpers\ProductServices\CreateProductService;
use App\Helpers\ProductServices\UpdateProductService;


class ProductServicesController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');
		$args = $request->input('args');


		$baseQuery = ProductServices::with(['ProductServicesDetails',
                                            'ProductServicesDetails.ProductServicesType',
                                            'ProductServicesStatus',
								            'ProductServicesDetails.ProductServicesAccounting.AccountingDetails']);

		$count = $baseQuery->count();
		$data = $baseQuery->take($per_page)->skip($skip)->get();
		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination,  'request'=>$request->all()]);
    }

    public function create(Request $request){
    	$product_services_details = $request->input('product_services_details');
    	$product_services_accounting = $request->input('product_services_accounting');

    	$data = ['product_services_details'=>$product_services_details,
    			 'product_services_accounting'=>$product_services_accounting,
    			];

    	$CreateProductService = new CreateProductService($data);
    	$result = $CreateProductService->init();

    	return response()->json(['data'=>$result]);
    }

    public function get(Request $request){
    	$id = $request->input('id');

    	$data = ProductServices::with(['ProductServicesDetails',
                                       'ProductServicesDetails.ProductServicesType',
                                       'ProductServicesStatus',
								       'ProductServicesDetails.ProductServicesAccounting.AccountingDetails'])->find($id);
    	return response()->json(['data'=>$data]);
    }

    public function update(Request $request){
    	$product_services_details = $request->input('product_services_details');
    	$product_services_accounting = $request->input('product_services_accounting');
    	$product_id = $request->input('product_id');

    	$data = [
    			 'product_id'=>$product_id,
    			 'product_services_details'=>$product_services_details,
    			 'product_services_accounting'=>$product_services_accounting,
    			];

    	$UpdateProductService = new UpdateProductService($data);
    	$result = $UpdateProductService->init();

    	return response()->json(['data'=>$data]);
    }

    public function delete(Request $request){
    	$id = $request->input('id');

    	$data = ProductServices::with(['ProductServicesDetails',
                                       'ProductServicesDetails.ProductServicesType',
                                       'ProductServicesStatus',
								       'ProductServicesDetails.ProductServicesAccounting.AccountingDetails'])->find($id);

    	if(!empty($data)){
    		$data->delete();
    	}
    	
    	return response()->json(['data'=>$data]);
    }

}
