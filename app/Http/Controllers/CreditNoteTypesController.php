<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\CreditNoteTypes;


class CreditNoteTypesController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');

		$baseQuery = CreditNoteTypes::query();

		$count = $baseQuery->count();

		$data = $baseQuery->take($per_page)->skip($skip)->get();

		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination]);
    }

}
