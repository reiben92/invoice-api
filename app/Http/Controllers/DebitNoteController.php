<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\DebitNotes;
use App\Helpers\DebitNotes\CreateDebitNote;
use App\Helpers\DebitNotes\UpdateDebitNote;


class DebitNoteController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');
		$args = $request->input('args');

        $customer_id = '';

        if(!empty($args['customer_id'])){
            $customer_id = $args['customer_id'];
        }

		$baseQuery = DebitNotes::with(['DebitNoteCustomers',
								'DebitNoteCustomers.Customer.CustomerDetails',
			                    'Status',
			                    'DebitNoteDetails',
			                    'DebitNoteItems',
			                    'DebitNoteItems.DebitNoteType',
								'DebitNoteItems.DebitNoteTaxes',
								'DebitNoteStatementOfAccount'])
                                ->when($customer_id,function ($query) use ($customer_id){
                                    $query->whereHas('DebitNoteCustomers',function ($query) use ($customer_id){
                                        $query->where('customer_id',$customer_id);
                                    });
                                });


		$count = $baseQuery->count();
		$data = $baseQuery->take($per_page)->skip($skip)->get();
		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination,  'request'=>$request->all()]);
    }

    public function create(Request $request){
    	$data = $request->all();

        $code = 500; $err_msg = '';

    	$debit_note = $request->input('debit_note');
    	$debit_note_items = $request->input('debit_note_items');
    	$debit_note_summary = $request->input('debit_note_summary');
    	$debit_note_customer = $request->input('debit_note_customer');
    	$debit_note_details = $request->input('debit_note_details');

        if(empty($debit_note_customer['id'])){
            $err_msg = 'Empty customer. Please select a customer.';
        }
        else{
            if(empty($debit_note_items)){
                $err_msg = 'Empty items. Please input items.';
            }
            else{
                $data = ['debit_note'=>$debit_note,
                         'debit_note_items'=>$debit_note_items,
                         'debit_note_summary'=>$debit_note_summary,
                         'debit_note_details'=>$debit_note_details,
                         'debit_note_customer'=>$debit_note_customer
                        ];

                $CreateDebitNote = new CreateDebitNote($data);
                $debit_note = $CreateDebitNote->init();
                $code = 200;
            }
        }

    	
    	
    	return response()->json(['data'=>$data, 'debit_note'=>$debit_note ,'err_msg'=>$err_msg, 'code'=>$code]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $data = DebitNotes::with(['DebitNoteCustomers',
                                'DebitNoteCustomers.Customer.CustomerDetails',
                                'Status',
                                'DebitNoteDetails',
                                'DebitNoteItems',
                                'DebitNoteSummary',
                                'DebitNoteItems.DebitNoteType',
                                'DebitNoteItems.DebitNoteTaxes',
                            ]
                            )->find($id);

        return response()->json(['data'=>$data]);
    }

    public function update(Request $request){
        $data = $request->all();

        $code = 500; $err_msg = '';

        $debit_note = $request->input('debit_note');
        $debit_note_items = $request->input('debit_note_items');
        $debit_note_summary = $request->input('debit_note_summary');
        $debit_note_customer = $request->input('debit_note_customer');
        $debit_note_details = $request->input('debit_note_details');
        $deleted_debit_note_items = $request->input('deleted_debit_note_items');

        if(empty($debit_note_customer['customer_id'])){
            $err_msg = 'Empty customer. Please select a customer.';
        }
        else{
            if(empty($debit_note_items)){
                $err_msg = 'Empty items. Please input items.';
            }
            else{
                $code = 200;
                $data = ['debit_note'=>$debit_note,
                         'debit_note_items'=>$debit_note_items,
                         'debit_note_summary'=>$debit_note_summary,
                         'debit_note_details'=>$debit_note_details,
                         'debit_note_customer'=>$debit_note_customer,
                         'debit_note_quotation'=>$debit_note_quotation,
                         'debit_note_purchase_order'=>$debit_note_purchase_order,
                         'deleted_debit_note_items'=>$deleted_debit_note_items
                        ];

                $UpdateDebitNote = new UpdateDebitNote($data);
                $response = $UpdateDebitNote->init();
            }
        }

        return response()->json(['data'=>$data,'err_msg'=>$err_msg, 'code'=>$code]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $data = DebitNotes::with(['DebitNoteCustomers',
                                'DebitNoteCustomers.Customer.CustomerDetails',
                                'Status',
                                'DebitNoteDetails',
                                'DebitNoteItems',
                                'DebitNoteSummary',
                                'DebitNoteItems.DebitNoteType',
                                'DebitNoteItems.DebitNoteTaxes',
                            ]
                            )->find($id);
        if(!empty($data)){
            $data->delete();
        }

        return response()->json(['data'=>$data]);
    }
}
