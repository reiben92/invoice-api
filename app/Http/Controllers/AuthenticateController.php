<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use JWTAuth;
use Auth;

class AuthenticateController extends Controller
{	

    public function authenticate(Request $request){
    	$token = null;
        $credentials = $request->only('client_id', 'client_secret');

        if(empty($credentials['client_id'])){$credentials['client_id']=null;}
        if(empty($credentials['client_secret'])){$credentials['client_secret']=null;}


        $clients = Clients::where('client_id',$credentials['client_id'])->first();

        if(!empty($clients)){
            if (! $token = Auth::guard('clients')->attempt(['client_id' => $credentials['client_id'], 'password' => $credentials['client_secret']])) {
                return response()->json(['msg' => 'Unauthorized','access_token'=>$token ], 200);
            }
            else{
                return $this->respondWithToken($token);
            }
        }
        else{
        	return response()->json(['msg'=>'Missing credentials', 'access_token'=>$token]);
        }
    }

    protected function respondWithToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('clients')->factory()->getTTL() * 60,
            'msg'=>'Success',
        ]);
    }

    public function validateToken(Request $request){
            $data = 'Valid token';

            return response()->json(['code'=>200, 'msg'=>$data]);
        }
}
