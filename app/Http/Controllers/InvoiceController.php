<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\Invoices;
use App\Helpers\Invoices\CreateInvoice;
use App\Helpers\Invoices\UpdateInvoice;
use App\Helpers\Recurring\CreateRecurring;
use App\Libs\Validation\InvoiceValidation;


class InvoiceController extends Controller
{	
    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');
		$args = $request->input('args');

        $customer_id='';
        $invoice_status_id='';

        $get_issued_invoices = true;

        if(!empty($args['customer_id'])){
            $customer_id = $args['customer_id'];
        }

        if(!empty($args['invoice_status_id'])){
            $invoice_status_id = $args['invoice_status_id'];
            $get_issued_invoices = false;
        }

		$baseQuery = Invoices::with(['InvoiceCustomers',
								'InvoiceCustomers.Customer.CustomerDetails',
			                    'Status',
                                'InvoiceSummary',
			                    'InvoiceDetails',
			                    'InvoiceItems',
			                    'InvoiceItems.InvoiceType',
								'InvoiceItems.InvoiceTaxes',
								'InvoiceStatementOfAccount'])
                                ->when($customer_id,function ($query) use ($customer_id){
                                    $query->whereHas('InvoiceCustomers',function ($query) use ($customer_id){
                                        $query->where('customer_id',$customer_id);
                                    });
                                })
                                ->when($invoice_status_id,function ($query) use ($invoice_status_id){
                                    $query->where('invoice_status_id',$invoice_status_id);
                                })
                                ->when($get_issued_invoices,function($query){
                                    $query->where('invoice_status_id','>',1);
                                });
                                


		$count = $baseQuery->count();
		$data = $baseQuery->take($per_page)->skip($skip)->orderBy('id','desc')->get();
		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination,  'request'=>$request->all(), 'get_issued_invoices'=>$get_issued_invoices]);
    }

    public function create(Request $request){
    	$data = $request->all();

        $code = 500; $err_msg = '';

    	$invoice = $request->input('invoice');
    	$invoice_items = $request->input('invoice_items');
    	$invoice_summary = $request->input('invoice_summary');
    	$invoice_customer = $request->input('invoice_customer');
    	$invoice_quotation = $request->input('invoice_quotation');
    	$invoice_purchase_order = $request->input('invoice_purchase_order');
    	$invoice_details = $request->input('invoice_details');
        $invoice_recurring = $request->input('invoice_recurring');

        $data = ['invoice'=>$invoice,
                 'invoice_items'=>$invoice_items,
                 'invoice_summary'=>$invoice_summary,
                 'invoice_details'=>$invoice_details,
                 'invoice_customer'=>$invoice_customer,
                 'invoice_quotation'=>$invoice_quotation,
                 'invoice_purchase_order'=>$invoice_purchase_order,
                 'invoice_recurring'=>$invoice_recurring,
                ];


        $validation = new InvoiceValidation($data);
        $validation_response = $validation->validate();

        $code = $validation_response['code'];

        if($code==200){
            if(empty($invoice_recurring['interval'])){
                $CreateInvoice = new CreateInvoice($data);
                $invoice = $CreateInvoice->init();
            }
            else{
                $CreateInvoice = new CreateInvoice($data);
                $invoice = $CreateInvoice->init();

                $Recurring = new CreateRecurring($data);
                $Recurring->init();
            }
        }
        else{
            $err_msg = $validation_response['err_msg'];
        }
    	
    	return response()->json(['data'=>$data, 'invoice'=>$invoice ,'err_msg'=>$err_msg, 'code'=>$code]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $data = Invoices::with(['InvoiceCustomers',
                                'InvoiceCustomers.Customer.CustomerDetails',
                                'Status',
                                'InvoiceDetails',
                                'InvoiceItems',
                                'InvoiceSummary',
                                'InvoiceItems.InvoiceType',
                                'InvoiceItems.InvoiceTaxes',
                                'InvoiceStatementOfAccount',
                                'InvoiceQuotation',
                                'InvoicePurchaseOrder',
                            ]
                            )->find($id);

        return response()->json(['data'=>$data]);
    }

    public function update(Request $request){
        $data = $request->all();

        $code = 500; $err_msg = '';

        $invoice = $request->input('invoice');
        $invoice_items = $request->input('invoice_items');
        $invoice_summary = $request->input('invoice_summary');
        $invoice_customer = $request->input('invoice_customer');
        $invoice_quotation = $request->input('invoice_quotation');
        $invoice_purchase_order = $request->input('invoice_purchase_order');
        $invoice_details = $request->input('invoice_details');
        $deleted_invoice_items = $request->input('deleted_invoice_items');

        $data = ['invoice'=>$invoice,
                 'invoice_items'=>$invoice_items,
                 'invoice_summary'=>$invoice_summary,
                 'invoice_details'=>$invoice_details,
                 'invoice_customer'=>$invoice_customer,
                 'invoice_quotation'=>$invoice_quotation,
                 'invoice_purchase_order'=>$invoice_purchase_order,
                 'deleted_invoice_items'=>$deleted_invoice_items
                ];

        $validation = new InvoiceValidation($data);
        $validation_response = $validation->validate();

        $code = $validation_response['code'];

        if($code==200){
            $UpdateInvoice = new UpdateInvoice($data);
            $response = $UpdateInvoice->init();
            
        }
        else{
            $err_msg = $validation_response['err_msg'];
        }

        return response()->json(['data'=>$data,'err_msg'=>$err_msg, 'code'=>$code]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $data = Invoices::with(['InvoiceCustomers',
                                'InvoiceCustomers.Customer.CustomerDetails',
                                'Status',
                                'InvoiceDetails',
                                'InvoiceItems',
                                'InvoiceSummary',
                                'InvoiceItems.InvoiceType',
                                'InvoiceItems.InvoiceTaxes',
                                'InvoiceStatementOfAccount',
                                'InvoiceQuotation',
                                'InvoicePurchaseOrder',
                            ]
                            )->find($id);
        if(!empty($data)){
            $data->delete();
        }

        return response()->json(['data'=>$data]);
    }
}
