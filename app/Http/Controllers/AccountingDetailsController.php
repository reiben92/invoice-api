<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\AccountingDetails;


class AccountingDetailsController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');
		$args = $request->input('args');

		$baseQuery = AccountingDetails::query();

		$count = $baseQuery->count();
		$data = $baseQuery->take($per_page)->skip($skip)->get();
		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination,  'request'=>$request->all()]);
    }

    public function create(Request $request){
		$accounting = $request->input('accounting');
		$AccountingDetails = [];
		$code = 404;
		$err_msg = '';

		if(!empty($accounting['title'])){
			$AccountingDetails = new AccountingDetails;
			$AccountingDetails->title = $accounting['title'];
			$AccountingDetails->save();
			$code = 200;
		}
		else{
			$code = 401;
			$err_msg = 'Missing title parameter.';
		}

    	return response()->json(['data'=>$AccountingDetails, 'request'=>$accounting, 'code'=>$code, 'err_msg'=>$err_msg]);
    }
}
