<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\CreditNotes;
use App\Helpers\CreditNotes\CreateCreditNote;
use App\Helpers\CreditNotes\UpdateCreditNote;


class CreditNoteController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');
		$args = $request->input('args');

        $customer_id = '';

        if(!empty($args['customer_id'])){
            $customer_id = $args['customer_id'];
        }

		$baseQuery = CreditNotes::with(['CreditNoteCustomers',
								'CreditNoteCustomers.Customer.CustomerDetails',
			                    'Status',
			                    'CreditNoteDetails',
			                    'CreditNoteItems',
                                'CreditNoteSummary',
			                    'CreditNoteItems.CreditNoteTypes',
                                'CreditNoteItems.Invoices',
                                'CreditNoteItems.Receipts',
                                'CreditNoteItems.CreditNotes',
								'CreditNoteItems.CreditNoteTaxes'])
                                ->when($customer_id,function ($query) use ($customer_id){
                                    $query->whereHas('CreditNoteCustomers',function ($query) use ($customer_id){
                                        $query->where('customer_id',$customer_id);
                                    });
                                });


		$count = $baseQuery->count();
		$data = $baseQuery->take($per_page)->skip($skip)->get();
		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination,  'request'=>$request->all()]);
    }

    public function create(Request $request){
    	$data = $request->all();

        $code = 500; $err_msg = '';

    	$credit_note = $request->input('credit_note');
    	$credit_note_items = $request->input('credit_note_items');
    	$credit_note_summary = $request->input('credit_note_summary');
    	$credit_note_customer = $request->input('credit_note_customer');
    	$credit_note_details = $request->input('credit_note_details');

        if(empty($credit_note_customer['id'])){
            $err_msg = 'Empty customer. Please select a customer.';
        }
        else{
            if(empty($credit_note_items)){
                $err_msg = 'Empty items. Please input items.';
            }
            else{
                $data = ['credit_note'=>$credit_note,
                         'credit_note_items'=>$credit_note_items,
                         'credit_note_summary'=>$credit_note_summary,
                         'credit_note_details'=>$credit_note_details,
                         'credit_note_customer'=>$credit_note_customer
                        ];

                $CreateCreditNote = new CreateCreditNote($data);
                $credit_note = $CreateCreditNote->init();
                $code = 200;
            }
        }

    	
    	
    	return response()->json(['data'=>$data, 'credit_note'=>$credit_note ,'err_msg'=>$err_msg, 'code'=>$code]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $data = CreditNotes::with(['CreditNoteCustomers',
                                'CreditNoteCustomers.Customer.CustomerDetails',
                                'Status',
                                'CreditNoteDetails',
                                'CreditNoteItems',
                                'CreditNoteSummary',
                                'CreditNoteItems.CreditNoteTypes',

                                'CreditNoteItems.CreditNoteTaxes'])->find($id);

        return response()->json(['data'=>$data]);
    }

    public function update(Request $request){
        $data = $request->all();

        $code = 500; $err_msg = '';

        $credit_note = $request->input('credit_note');
        $credit_note_items = $request->input('credit_note_items');
        $credit_note_summary = $request->input('credit_note_summary');
        $credit_note_customer = $request->input('credit_note_customer');
        $credit_note_details = $request->input('credit_note_details');
        $deleted_credit_note_items = $request->input('deleted_credit_note_items');

        if(empty($credit_note_customer['customer_id'])){
            $err_msg = 'Empty customer. Please select a customer.';
        }
        else{
            if(empty($credit_note_items)){
                $err_msg = 'Empty CreditNote items. Please input CreditNote items.';
            }
            else{
                $code = 200;
                $data = ['credit_note'=>$credit_note,
                         'credit_note_items'=>$credit_note_items,
                         'credit_note_summary'=>$credit_note_summary,
                         'credit_note_details'=>$credit_note_details,
                         'credit_note_customer'=>$credit_note_customer,
                         'deleted_credit_note_items'=>$deleted_credit_note_items
                        ];

                $UpdateCreditNote = new UpdateCreditNote($data);
                $response = $UpdateCreditNote->init();
            }
        }

        return response()->json(['data'=>$data,'err_msg'=>$err_msg, 'code'=>$code]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $data = CreditNotes::find($id);

        if(!empty($data)){
            $data->delete();
        }

        return response()->json(['data'=>$data]);
    }
}
