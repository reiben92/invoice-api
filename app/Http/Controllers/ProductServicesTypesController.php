<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\ProductServicesTypes;


class ProductServicesTypesController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');

		$baseQuery = ProductServicesTypes::query();

		$count = $baseQuery->count();

		$data = $baseQuery->take($per_page)->skip($skip)->get();

		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination]);
    }

    public function get(Request $request){
		$id = $request->input('id');
		$code = 200;
		$err_msg='';

		$ProductServicesTypes = ProductServicesTypes::find($id);

		if(empty($ProductServicesTypes)){
			$code = 404;
			$err_msg='Item not found.';
		}

    	return response()->json(['data'=>$ProductServicesTypes, 'code'=>$code, 'err_msg'=>$err_msg]);
    }

    public function create(Request $request){
		$product_services_type = $request->input('product_services_type');
		$ProductServicesTypes = [];
		$code = 404;
		$err_msg = '';

		if(!empty($product_services_type['title'])){
			$ProductServicesTypes = new ProductServicesTypes;
			$ProductServicesTypes->title = $product_services_type['title'];
			$ProductServicesTypes->description = $product_services_type['description'];
			$ProductServicesTypes->save();
			$code = 200;
		}
		else{
			$code = 401;
			$err_msg = 'Missing title parameter.';
		}

    	return response()->json(['data'=>$ProductServicesTypes, 'request'=>$product_services_type, 'code'=>$code, 'err_msg'=>$err_msg]);
    }

    public function update(Request $request){
		$product_services_type = $request->input('product_services_type');
		$ProductServicesTypes = [];
		$code = 404;
		$err_msg = '';

		if(!empty($product_services_type['title'])){
			$ProductServicesTypes = ProductServicesTypes::find($product_services_type['id']);
			if(!empty($ProductServicesTypes)){
				$ProductServicesTypes->title = $product_services_type['title'];
				$ProductServicesTypes->description = $product_services_type['description'];
				$ProductServicesTypes->save();
				$err_msg = '';
				$code = 200;
			}
			else{
				$err_msg = 'Item not found.';
			}
		}
		else{
			$code = 401;
			$err_msg = 'Missing title parameter.';
		}

    	return response()->json(['data'=>$ProductServicesTypes, 'request'=>$product_services_type, 'code'=>$code, 'err_msg'=>$err_msg]);
    }

    public function delete(Request $request){
		$id = $request->input('id');
		$code = 404;
		$err_msg='Item not found.';

		$ProductServicesTypes = ProductServicesTypes::find($id);

		if(!empty($ProductServicesTypes)){
			$ProductServicesTypes->delete();
			$code = 200;
			$err_msg='';
		}

    	return response()->json(['data'=>$ProductServicesTypes, 'code'=>$code, 'err_msg'=>$err_msg]);
    }
}
