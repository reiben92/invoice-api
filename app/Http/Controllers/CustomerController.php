<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customers;
use App\Helpers\Customers\CreateCustomer;
use App\Helpers\Customers\UpdateCustomer;

class CustomerController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');

		$baseQuery = Customers::with(['CustomerAddresses','CustomerDetails','CustomerContacts','CustomerBalances']);

		$count = $baseQuery->count();

		$data = $baseQuery->take($per_page)->skip($skip)->orderBy('id','desc')->get();

		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination,  'request'=>$request->all()]);
    }

    public function create(Request $request){
    	$customer_details = $request->input('customer_details');
    	$customer_addresses = $request->input('customer_addresses');
    	$customer_balances = $request->input('customer_balances');
        $customer_contacts = $request->input('customer_contacts');

    	$data = ['customer_details'=>$customer_details,
    			 'customer_addresses'=>$customer_addresses,
    			 'customer_balances'=>$customer_balances,
    			 'customer_contacts'=>$customer_contacts,
                ];

    	$CreateCustomer = new CreateCustomer($data);
        $CreateCustomer->init();

    	return response()->json(['data'=>$data]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $data = Customers::with(['CustomerAddresses','CustomerDetails','CustomerContacts','CustomerBalances'])->find($id);
        

        return response()->json(['data'=>$data, 'request'=>$request->all()]);
    }

    public function update(Request $request){
        $customer_details = $request->input('customer_details');
        $customer_addresses = $request->input('customer_addresses');
        $customer_balances = $request->input('customer_balances');
        $customer_contacts = $request->input('customer_contacts');
        $customer_id = $request->input('customer_id');

        $data = ['customer_details'=>$customer_details,
                 'customer_addresses'=>$customer_addresses,
                 'customer_balances'=>$customer_balances,
                 'customer_contacts'=>$customer_contacts,
                 'customer_id'=>$customer_id,
                ];

        $UpdateCustomer = new UpdateCustomer($data);
        $UpdateCustomer->init();

        return response()->json(['data'=>$data]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $data = Customers::with(['CustomerAddresses','CustomerDetails','CustomerContacts','CustomerBalances'])->find($id);

        if(!empty($data)){
            $data->delete();
        }

        return response()->json(['data'=>$data, 'request'=>$request->all()]);
    }
}
