<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\Currencies;


class CurrencyController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');

		$baseQuery = Currencies::query();

		$count = $baseQuery->count();

		$data = $baseQuery->take($per_page)->skip($skip)->get();

		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $Currencies = Currencies::find($id);

        return response()->json(['data'=>$Currencies]);
    }

    public function create(Request $request){
    	$data = $request->input('currency');

        $Currencies = new Currencies;
        $Currencies->title = $data['title'];
        $Currencies->code = $data['code'];
        $Currencies->save();

    	return response()->json(['data'=>$Currencies]);
    }

    public function update(Request $request){
        $data = $request->input('currency');
        $code = 500;
        $err_msg = '';

        $Currencies = Currencies::find($data['id']);
        if(!empty($Currencies)){
            $Currencies->title = $data['title'];
            $Currencies->description = $data['code'];
            $Currencies->save();
            $code = 200;
        }
        else{
            $code = 404; $err_msg = 'Currency not found.';
        }

        return response()->json(['data'=>$Currencies, 'code'=>$code, 'err_msg'=>$err_msg ]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $Currencies = Currencies::find($id);

        if(!empty($id)){$Currencies->delete();}

        return response()->json(['data'=>$Currencies]);
    }
}
