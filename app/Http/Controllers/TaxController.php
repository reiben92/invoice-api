<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\Taxes;


class TaxController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');

		$baseQuery = Taxes::query();

		$count = $baseQuery->count();

		$data = $baseQuery->take($per_page)->skip($skip)->get();

		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $taxes = Taxes::find($id);

        return response()->json(['data'=>$taxes]);
    }

    public function create(Request $request){
    	$data = $request->input('taxes');

        $taxes = new Taxes;
        $taxes->title = $data['title'];
        $taxes->description = $data['description'];
        $taxes->default_tax_rate = $data['default_tax_rate'];
        $taxes->save();

    	return response()->json(['data'=>$taxes]);
    }

    public function update(Request $request){
        $data = $request->input('taxes');
        $code = 500;
        $err_msg = '';

        $taxes = Taxes::find($data['id']);
        if(!empty($taxes)){
            $taxes->title = $data['title'];
            $taxes->description = $data['description'];
            $taxes->default_tax_rate = $data['default_tax_rate'];
            $taxes->save();
            $code = 200;
        }
        else{
            $code = 404; $err_msg = 'Tax not found.';
        }

        return response()->json(['data'=>$taxes, 'code'=>$code, 'err_msg'=>$err_msg ]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $taxes = Taxes::find($id);

        if(!empty($id)){$taxes->delete();}

        return response()->json(['data'=>$taxes]);
    }
}
