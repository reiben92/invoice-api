<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\PaymentMethods;


class PaymentMethodController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');

		$baseQuery = PaymentMethods::query();

		$count = $baseQuery->count();

		$data = $baseQuery->take($per_page)->skip($skip)->get();

		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $PaymentMethods = PaymentMethods::find($id);

        return response()->json(['data'=>$PaymentMethods]);
    }

    public function create(Request $request){
    	$data = $request->input('payment_methods');

        $PaymentMethods = new PaymentMethods;
        $PaymentMethods->title = $data['title'];
        $PaymentMethods->value = $data['value'];
        $PaymentMethods->save();

    	return response()->json(['data'=>$PaymentMethods]);
    }

    public function update(Request $request){
        $data = $request->input('payment_methods');
        $code = 500;
        $err_msg = '';

        $PaymentMethods = PaymentMethods::find($data['id']);
        if(!empty($PaymentMethods)){
            $PaymentMethods->title = $data['title'];
            $PaymentMethods->value = $data['value'];
            $PaymentMethods->save();
            $code = 200;
        }
        else{
            $code = 404; $err_msg = 'Currency not found.';
        }

        return response()->json(['data'=>$PaymentMethods, 'code'=>$code, 'err_msg'=>$err_msg ]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $PaymentMethods = PaymentMethods::find($id);

        if(!empty($id)){$PaymentMethods->delete();}

        return response()->json(['data'=>$PaymentMethods]);
    }
}
