<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Models\Receipts;
use App\Helpers\Receipts\CreateReceipt;
use App\Helpers\Receipts\UpdateReceipt;


class ReceiptController extends Controller
{	

    public function list(Request $request){
		$per_page = $request->input('per_page'); 
		$skip = $request->input('skip');
		$args = $request->input('args');

        $customer_id = '';

        if(!empty($args['customer_id'])){
            $customer_id = $args['customer_id'];
        }

		$baseQuery = Receipts::with(['ReceiptCustomers',
								'ReceiptCustomers.Customer.CustomerDetails',
			                    'Status',
			                    'ReceiptDetails',
			                    'ReceiptItems',
                                'ReceiptItems.Invoices',
			                    'ReceiptItems.ReceiptType',
								'ReceiptItems.ReceiptTaxes',
								'ReceiptSummary'])
                                ->when($customer_id,function ($query) use ($customer_id){
                                    $query->whereHas('ReceiptCustomers',function ($query) use ($customer_id){
                                        $query->where('customer_id',$customer_id);
                                    });
                                });


		$count = $baseQuery->count();
		$data = $baseQuery->take($per_page)->skip($skip)->orderBy('id','desc')->get();
		$pagination = ceil($count / $per_page);

    	return response()->json(['data'=>$data, 'count'=>$count, 'pagination'=>$pagination,  'request'=>$request->all()]);
    }

    public function create(Request $request){
        $data = $request->all();

        $err_msg = ''; $code = 500;
    	$receipt = $request->input('receipt');
    	$receipt_items = $request->input('receipt_items');
    	$receipt_summary = $request->input('receipt_summary');
    	$receipt_customer = $request->input('receipt_customer');
    	$receipt_details = $request->input('receipt_details');

        if(empty($receipt_customer['customer_id'])){
            $err_msg = 'Empty customer. Please select a customer.';
        }
        else{
            if(empty($receipt_items)){
                $err_msg = 'Empty receipt items. Please input receipt items.';
            }
            else{
            	$data = ['receipt'=>$receipt,
            			 'receipt_items'=>$receipt_items,
            			 'receipt_summary'=>$receipt_summary,
            			 'receipt_details'=>$receipt_details,
            			 'receipt_customer'=>$receipt_customer,
            			];
            
                $code = 200;
                
                $CreateReceipt = new CreateReceipt($data);
                $receipt = $CreateReceipt->init();
            }
        }
    	
    	return response()->json(['data'=>$data, 'receipt'=>$receipt ,'code'=>$code, 'err_msg'=>$err_msg]);
    }

    public function get(Request $request){
        $id = $request->input('id');

        $data = Receipts::with(['ReceiptCustomers',
                                'ReceiptCustomers.Customer.CustomerDetails',
                                'Status',
                                'ReceiptDetails',
                                'ReceiptItems',
                                'ReceiptSummary',
                                'ReceiptItems.ReceiptType',
                                'ReceiptItems.ReceiptTaxes',
                                'ReceiptItems.Invoices',
                            ]
                            )->find($id);

        return response()->json(['data'=>$data]);
    }

    public function update(Request $request){
        $code = 500; $err_msg='';
        $data = $request->all();

        $receipt = $request->input('receipt');
        $receipt_items = $request->input('receipt_items');
        $receipt_summary = $request->input('receipt_summary');
        $receipt_customer = $request->input('receipt_customer');
        $receipt_details = $request->input('receipt_details');
        $deleted_receipt_items = $request->input('deleted_receipt_items');

        if(empty($receipt_customer['customer_id'])){
            $err_msg = 'Empty customer. Please select a customer.';
        }
        else{
            if(empty($receipt_items)){
                $err_msg = 'Empty receipt items. Please input receipt items.';
            }
            else{
                $data = ['receipt'=>$receipt,
                         'receipt_items'=>$receipt_items,
                         'receipt_summary'=>$receipt_summary,
                         'receipt_details'=>$receipt_details,
                         'receipt_customer'=>$receipt_customer,
                         'deleted_receipt_items'=>$deleted_receipt_items
                        ];

                $UpdateReceipt = new UpdateReceipt($data);
                $response = $UpdateReceipt->init();
                $code = 200;
            }
        }
        
        return response()->json(['data'=>$data, 'code'=>$code, 'err_msg'=>$err_msg]);
    }

    public function delete(Request $request){
        $id = $request->input('id');

        $data = Receipts::with(['ReceiptCustomers',
                                'ReceiptCustomers.Customer.CustomerDetails',
                                'Status',
                                'ReceiptDetails',
                                'ReceiptItems',
                                'ReceiptSummary',
                                'ReceiptItems.ReceiptType',
                                'ReceiptItems.ReceiptTaxes',
                            ]
                            )->find($id);

        if(!empty($data)){
            $data->delete();
        }

        return response()->json(['data'=>$data]);
    }
}
