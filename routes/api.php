<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/authenticate','AuthenticateController@authenticate');	

Route::group(['middleware' => ['jwt.verify']], function(){
	Route::post('/validate','AuthenticateController@validateToken');	
	Route::post('/invoices/list','InvoiceController@list');
	Route::post('/invoices/create','InvoiceController@create');
	Route::post('/invoices/get','InvoiceController@get');
	Route::post('/invoices/update','InvoiceController@update');
	Route::post('/invoices/delete','InvoiceController@delete');

	Route::post('/credit_note/list','CreditNoteController@list');
	Route::post('/credit_note/create','CreditNoteController@create');
	Route::post('/credit_note/get','CreditNoteController@get');
	Route::post('/credit_note/update','CreditNoteController@update');
	Route::post('/credit_note/delete','CreditNoteController@delete');

	Route::post('/credit_note/types/list','CreditNoteTypesController@list');

	Route::post('/debit_note/list','DebitNoteController@list');
	Route::post('/debit_note/create','DebitNoteController@create');
	Route::post('/debit_note/get','DebitNoteController@get');
	Route::post('/debit_note/update','DebitNoteController@update');
	Route::post('/debit_note/delete','DebitNoteController@delete');

	Route::post('/receipts/list','ReceiptController@list');
	Route::post('/receipts/create','ReceiptController@create');
	Route::post('/receipts/get','ReceiptController@get');
	Route::post('/receipts/update','ReceiptController@update');
	Route::post('/receipts/delete','ReceiptController@delete');

	Route::post('/taxes/get','TaxController@get');
	Route::post('/taxes/update','TaxController@update');
	Route::post('/taxes/create','TaxController@create');
	Route::post('/taxes/delete','TaxController@delete');
	Route::post('/taxes/list','TaxController@list');

	Route::post('/customers/list','CustomerController@list');
	Route::post('/customers/create','CustomerController@create');
	Route::post('/customers/get','CustomerController@get');
	Route::post('/customers/delete','CustomerController@delete');
	Route::post('/customers/update','CustomerController@update');

	Route::post('/product_services/list','ProductServicesController@list');
	Route::post('/product_services/get','ProductServicesController@get');
	Route::post('/product_services/update','ProductServicesController@update');
	Route::post('/product_services/create','ProductServicesController@create');
	Route::post('/product_services/delete','ProductServicesController@delete');

	Route::post('/product_services/types/list','ProductServicesTypesController@list');
	Route::post('/product_services/types/create','ProductServicesTypesController@create');
	Route::post('/product_services/types/get','ProductServicesTypesController@get');
	Route::post('/product_services/types/update','ProductServicesTypesController@update');
	Route::post('/product_services/types/delete','ProductServicesTypesController@delete');

	Route::post('/accounting_codes/list','AccountingDetailsController@list');
	Route::post('/accounting_codes/create','AccountingDetailsController@create');

	Route::post('/payment_method/get','PaymentMethodController@get');
	Route::post('/payment_method/update','PaymentMethodController@update');
	Route::post('/payment_method/create','PaymentMethodController@create');
	Route::post('/payment_method/delete','PaymentMethodController@delete');
	Route::post('/payment_method/list','PaymentMethodController@list');

	Route::post('/currencies/get','CurrencyController@get');
	Route::post('/currencies/update','CurrencyController@update');
	Route::post('/currencies/create','CurrencyController@create');
	Route::post('/currencies/delete','CurrencyController@delete');
	Route::post('/currencies/list','CurrencyController@list');	
});